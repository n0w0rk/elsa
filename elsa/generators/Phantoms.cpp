#include "Phantoms.h"
#include "EllipseGenerator.h"
#include "Logger.h"
#include "VolumeDescriptor.h"
#include "CartesianIndices.h"
#include "ForbildPhantom.h"
#include "ForbildData.h"
#include "Ellipsoid.h"
#include "Blobs.h"

#include <cmath>
#include <stdexcept>

namespace elsa::phantoms
{

    // scale sizes from [0,1] to the (square) phantom size, producing indices (integers)
    template <typename data_t>
    index_t scale(const DataDescriptor& dd, data_t value)
    {
        return std::lround(
            value * static_cast<data_t>(dd.getNumberOfCoefficientsPerDimension()[0] - 1) / 2.0f);
    }

    // scale and shift center coordinates to the (square) phantom size, producing indices
    // (integers)
    template <typename data_t>
    index_t scaleShift(const DataDescriptor& dd, data_t value)
    {
        return std::lround(static_cast<double>(value)
                           * static_cast<double>(dd.getNumberOfCoefficientsPerDimension()[0] - 1)
                           / 2.0)
               + (dd.getNumberOfCoefficientsPerDimension()[0] / 2);
    }

    template <typename data_t>
    DataContainer<data_t> circular(IndexVector_t volumesize, data_t radius)
    {
        VolumeDescriptor dd(volumesize);
        DataContainer<data_t> dc(dd);
        dc = 0;

        const Vector_t<data_t> sizef = volumesize.template cast<data_t>();
        const auto center = (sizef.array() / 2).matrix();

        for (auto pos : CartesianIndices(volumesize)) {
            const Vector_t<data_t> p = pos.template cast<data_t>();
            if ((p - center).norm() <= radius) {
                dc(pos) = 1;
            }
        }

        return dc;
    }

    template <typename data_t>
    DataContainer<data_t> rectangle(IndexVector_t volumesize, IndexVector_t lower,
                                    IndexVector_t upper)
    {
        VolumeDescriptor dd(volumesize);
        DataContainer<data_t> dc(dd);
        dc = 0;

        for (auto pos : CartesianIndices(lower, upper)) {
            dc(pos) = 1;
        }

        return dc;
    }

    template <typename data_t>
    DataContainer<data_t> modifiedSheppLogan(IndexVector_t sizes)
    {
        // sanity check
        if (sizes.size() < 2 || sizes.size() > 3)
            throw InvalidArgumentError("phantom::modifiedSheppLogan: only 2d or 3d supported");
        if (sizes.size() == 2 && sizes[0] != sizes[1])
            throw InvalidArgumentError("phantom::modifiedSheppLogan: 2d size has to be square");
        if (sizes.size() == 3 && (sizes[0] != sizes[1] || sizes[0] != sizes[2]))
            throw InvalidArgumentError("phantom::modifiedSheppLogan: 3d size has to be cubed");

        Logger::get("phantom::modifiedSheppLogan")
            ->info("creating modified Shepp Logan phantom of size {}^{}", sizes[0], sizes.size());

        VolumeDescriptor dd(sizes);
        DataContainer<data_t> dc(dd);
        dc = 0;

        if (sizes.size() == 2) {
            EllipseGenerator<data_t>::drawFilledEllipse2d(dc, 1.0,
                                                          {scaleShift(dd, 0), scaleShift(dd, 0)},
                                                          {scale(dd, 0.69f), scale(dd, 0.92f)}, 0);
            EllipseGenerator<data_t>::drawFilledEllipse2d(
                dc, -0.8f, {scaleShift(dd, 0), scaleShift(dd, -0.0184f)},
                {scale(dd, 0.6624f), scale(dd, 0.8740f)}, 0);
            EllipseGenerator<data_t>::drawFilledEllipse2d(
                dc, -0.2f, {scaleShift(dd, 0.22f), scaleShift(dd, 0)},
                {scale(dd, 0.11f), scale(dd, 0.31f)}, -18);
            EllipseGenerator<data_t>::drawFilledEllipse2d(
                dc, -0.2f, {scaleShift(dd, -0.22f), scaleShift(dd, 0)},
                {scale(dd, 0.16f), scale(dd, 0.41f)}, 18);
            EllipseGenerator<data_t>::drawFilledEllipse2d(
                dc, 0.1f, {scaleShift(dd, 0), scaleShift(dd, 0.35f)},
                {scale(dd, 0.21f), scale(dd, 0.25)}, 0);
            EllipseGenerator<data_t>::drawFilledEllipse2d(
                dc, 0.1f, {scaleShift(dd, 0), scaleShift(dd, 0.1f)},
                {scale(dd, 0.046f), scale(dd, 0.046f)}, 0);
            EllipseGenerator<data_t>::drawFilledEllipse2d(
                dc, 0.1f, {scaleShift(dd, 0), scaleShift(dd, -0.1f)},
                {scale(dd, 0.046f), scale(dd, 0.046f)}, 0);
            EllipseGenerator<data_t>::drawFilledEllipse2d(
                dc, 0.1f, {scaleShift(dd, -0.08f), scaleShift(dd, -0.605f)},
                {scale(dd, 0.046f), scale(dd, 0.023f)}, 0);
            EllipseGenerator<data_t>::drawFilledEllipse2d(
                dc, 0.1f, {scaleShift(dd, 0), scaleShift(dd, -0.606f)},
                {scale(dd, 0.023f), scale(dd, 0.023f)}, 0);
            EllipseGenerator<data_t>::drawFilledEllipse2d(
                dc, 0.1f, {scaleShift(dd, 0.06f), scaleShift(dd, -0.605f)},
                {scale(dd, 0.023f), scale(dd, 0.046f)}, 0);
        }

        if (sizes.size() == 3) {
            for (std::array<data_t, 10> e : modifiedSheppLoganParameters<data_t>) {

                Vec3X<data_t> halfAxis{data_t(scale(dd, e[1])), data_t(scale(dd, e[2])),
                                       data_t(scale(dd, e[3]))};

                if (halfAxis[0] < 1 || halfAxis[1] < 1 || halfAxis[2] < 1 || e[0] == data_t(0)) {
                    Logger::get("phantom::modifiedSheppLogan")
                        ->warn(
                            "Ellipsoid will not be rendered, because of amplitude=0 or an invalid "
                            "half axis! amplitude {}, half axis ({},{},{}) ",
                            e[0], halfAxis[0], halfAxis[1], halfAxis[2]);
                    continue;
                }

                Ellipsoid<data_t> ellipsoid{
                    e[0],
                    {scaleShift(dd, e[4]), scaleShift(dd, e[5]), scaleShift(dd, e[6])},
                    halfAxis,
                    {e[7], e[8], e[9]}};
                Logger::get("phantom::modifiedSheppLogan")->info("rasterize {}", ellipsoid);
                rasterize<Blending::ADDITION>(ellipsoid, dd, dc);
            }
        }

        return dc;
    }

    template <typename data_t>
    DataContainer<data_t> smoothBlob(IndexVector_t sizes, double radius_manipulation)
    {
        // sanity check
        if (sizes.size() < 2 || sizes.size() > 3)
            throw InvalidArgumentError("phantom::smoothBlob: only 2d or 3d supported");
        if (sizes.size() == 2 && sizes[0] != sizes[1])
            throw InvalidArgumentError("phantom::smoothBlob: 2d size has to be square");
        if (sizes.size() == 3 && (sizes[0] != sizes[1] || sizes[0] != sizes[2]))
            throw InvalidArgumentError("phantom::smoothBlob: 3d size has to be cubed");

        Logger::get("phantom::smoothBlob")
            ->info("creating smooth Blob phantom of size {}^{}", sizes[0], sizes.size());

        VolumeDescriptor dd(sizes);
        DataContainer<data_t> dc(dd);
        dc = 0;

        auto radius = sizes[0] / 2;

#pragma omp parallel for
        for (index_t i = 0; i < dc.getSize(); i++) {
            const RealVector_t coord =
                dd.getCoordinateFromIndex(i).template cast<real_t>().array() + 0.5;
            data_t distance_from_center = (coord - dd.getLocationOfOrigin()).norm();
            dc[i] =
                blobs::blob_evaluate(distance_from_center, radius * radius_manipulation, 10.83f, 2);
        }

        return dc;
    }

    template <typename data_t>
    DataContainer<data_t> forbild(IndexVector_t sizes, ForbildPhantom<data_t>& fp)
    {
        // sanity checks
        if (sizes.size() != 3 || sizes[0] != sizes[1] || sizes[0] != sizes[2]) {
            throw InvalidArgumentError("phantom::forbild: 3d size has to be cubed");
        }

        VolumeDescriptor dd(sizes);
        DataContainer<data_t> dc(dd);
        dc = 0;

        for (int order = 0; order < fp.getMaxOrderIndex(); order++) {
            if (fp.getEllipsoids().count(order)) {
                // Ellipsoids
                auto e = fp.getEllipsoids().at(order);
                Logger::get("phantoms::forbild")->info("rasterize {}", e);
                rasterize<Blending::OVERWRITE>(e, dd, dc);
            } else if (fp.getEllipsoidsClippedX().count(order)) {
                // Ellipsoids clipped at x axis
                auto [el, clipping] = fp.getEllipsoidsClippedX().at(order);
                Logger::get("phantoms::forbild")->info("rasterize {} with clipping", el);
                rasterizeWithClipping<Blending::OVERWRITE>(el, dd, dc, clipping);
            } else if (fp.getSpheres().count(order)) {
                // Spheres
                auto s = fp.getSpheres().at(order);
                Logger::get("phantoms::forbild")->info("rasterize {}", s);
                rasterize<Blending::OVERWRITE>(s, dd, dc);
            } else if (fp.getEllipCylinders().count(order)) {
                // EllipCylinders
                auto e = fp.getEllipCylinders().at(order);
                Logger::get("phantoms::forbild")->info("rasterize {}", e);
                rasterize<Blending::OVERWRITE>(e, dd, dc);
            } else if (fp.getEllipCylindersFree().count(order)) {
                // EllipCylinders free
                auto e = fp.getEllipCylindersFree().at(order);
                Logger::get("phantoms::forbild")->info("rasterize {}", e);
                rasterize<Blending::OVERWRITE>(e, dd, dc);
            } else if (fp.getCylinders().count(order)) {
                // Cylinders
                auto c = fp.getCylinders().at(order);
                Logger::get("phantoms::forbild")->info("rasterize {}", c);
                rasterize<Blending::OVERWRITE>(c, dd, dc);
            } else if (fp.getCylindersFree().count(order)) {
                // Cylinders free
                auto cf = fp.getCylindersFree().at(order);
                Logger::get("phantoms::forbild")->info("rasterize {}", cf);
                rasterize<Blending::OVERWRITE>(cf, dd, dc);
            } else if (fp.getCones().count(order)) {
                // Cones
                auto c = fp.getCones().at(order);
                Logger::get("phantoms::forbild")->info("rasterize {}", c);
                //rasterize<Blending::OVERWRITE>(c, dd, dc);
            } else if (fp.getBoxes().count(order)) {
                // Boxes
                auto b = fp.getBoxes().at(order);
                Logger::get("phantoms::forbild")->info("rasterize {}", b);
                rasterize<Blending::OVERWRITE>(b, dd, dc);
            } else {
                Logger::get("phantoms::forbild")->warn("No object found for order index {}", order);
            }
        }

        return dc;
    }

    template <typename data_t>
    DataContainer<data_t> forbildHead(IndexVector_t sizes)
    {

        Logger::get("phantom::forbildHead")
            ->info("creating forbild of size {}^{}", sizes[0], sizes.size());
        // field view from http://ftp.imp.uni-erlangen.de/phantoms/head/head.html
        ForbildPhantom<data_t> fp{sizes[0] - 1, data_t(25.6f), forbilddata::maxOrderIndex_head};
        fp.addEllipsoids(forbilddata::ellipsoids_head<data_t>);
        fp.addEllipsoidsClippedX(forbilddata::ellipsoidsClippingX_head<data_t>);
        fp.addSpheres(forbilddata::spheres_head<data_t>);
        fp.addEllipCylinders(forbilddata::ellipCyls_head<data_t>);
        fp.addEllipCylindersFree(forbilddata::ellipCylsFree_head<data_t>);
        fp.addCylinders(forbilddata::cyls_head<data_t>);
        fp.addCylindersFree(forbilddata::cylsFree_head<data_t>);
        fp.addCones(forbilddata::cones_head<data_t>);
        fp.addBoxes(forbilddata::boxes_head<data_t>);
        return std::move(forbild<data_t>(sizes, fp));
    }

    template <typename data_t>
    DataContainer<data_t> forbildThorax(IndexVector_t sizes)
    {

        Logger::get("phantom::forbildThorax")
            ->info("creating forbild of size {}^{}", sizes[0], sizes.size());
        // field view from http://ftp.imp.uni-erlangen.de/phantoms/thorax/thorax.htm
        ForbildPhantom<data_t> fp{sizes[0] - 1, data_t(50.f), forbilddata::maxOrderIndex_thorax};
        fp.addEllipsoids(forbilddata::ellipsoids_thorax<data_t>);
        fp.addSpheres(forbilddata::spheres_thorax<data_t>);
        fp.addEllipCylinders(forbilddata::ellipCyls_thorax<data_t>);
        fp.addEllipCylindersFree(forbilddata::ellipCylsFree_thorax<data_t>);
        fp.addCylinders(forbilddata::cyls_thorax<data_t>);
        fp.addCylindersFree(forbilddata::cylsFree_thorax<data_t>);
        fp.addBoxes(forbilddata::boxes_thorax<data_t>);

        return std::move(forbild<data_t>(sizes, fp));
    }

    template <typename data_t>
    DataContainer<data_t> forbildAbdomen(IndexVector_t sizes)
    {

        Logger::get("phantom::forbildAbdomen")
            ->info("creating forbild of size {}^{}", sizes[0], sizes.size());
        // field view from http://ftp.imp.uni-erlangen.de/phantoms/abdomen/abdomen.html
        ForbildPhantom<data_t> fp{sizes[0] - 1, data_t(40.f), forbilddata::maxOrderIndex_abdomen};
        fp.addEllipsoids(forbilddata::ellipsoids_abdomen<data_t>);
        fp.addSpheres(forbilddata::spheres_abdomen<data_t>);
        fp.addEllipCylinders(forbilddata::ellipCyls_abdomen<data_t>);
        fp.addEllipCylindersFree(forbilddata::ellipCylsFree_abdomen<data_t>);
        fp.addCylinders(forbilddata::cyls_abdomen<data_t>);
        fp.addCylindersFree(forbilddata::cylsFree_abdomen<data_t>);
        fp.addBoxes(forbilddata::boxes_abdomen<data_t>);
        return std::move(forbild<data_t>(sizes, fp));
    }

    // ------------------------------------------
    // explicit template instantiation
    template DataContainer<float> circular<float>(IndexVector_t volumesize, float radius);
    template DataContainer<double> circular<double>(IndexVector_t volumesize, double radius);

    template DataContainer<float> modifiedSheppLogan<float>(IndexVector_t sizes);
    template DataContainer<double> modifiedSheppLogan<double>(IndexVector_t sizes);

    template DataContainer<float> forbildHead<float>(IndexVector_t sizes);
    template DataContainer<double> forbildHead<double>(IndexVector_t sizes);

    template DataContainer<float> forbildAbdomen<float>(IndexVector_t sizes);
    template DataContainer<double> forbildAbdomen<double>(IndexVector_t sizes);

    template DataContainer<float> forbildThorax<float>(IndexVector_t sizes);
    template DataContainer<double> forbildThorax<double>(IndexVector_t sizes);

    template DataContainer<float> smoothBlob<float>(IndexVector_t sizes,
                                                    double radius_manipulation);
    template DataContainer<double> smoothBlob<double>(IndexVector_t sizes,
                                                      double radius_manipulation);

    template DataContainer<float> rectangle<float>(IndexVector_t volumesize, IndexVector_t lower,
                                                   IndexVector_t upper);
    template DataContainer<double> rectangle<double>(IndexVector_t volumesize, IndexVector_t lower,
                                                     IndexVector_t upper);
} // namespace elsa::phantoms
