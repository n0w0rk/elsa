/**
 * @file test_Tetrahedron.cpp
 *
 * @brief Test for Tetrahedron
 */

#include "doctest/doctest.h"

#include "PhantomDefines.h"
#include "Tetrahedron.h"

using namespace elsa;
using namespace doctest;

TEST_CASE("tetrahedron tests")
{
    GIVEN("A shifted tetrahedron")
    {
        index_t size = 10;
        IndexVector_t sizes(3);
        sizes << size, size, size;

        phantoms::Vec3i center;
        center << 5.0, 5.0, 5.0;

        double amplit{1.};

        VolumeDescriptor dd(sizes);
        DataContainer<double> dc(dd);
        dc = 0;

        phantoms::Vec3i point1;
        point1 << 0, 5.0, 0;

        phantoms::Vec3i point2;
        point2 << 0, 5.0, 10.0;

        phantoms::Vec3i point3;
        point3 << 10.0, 0, 5.0;

        phantoms::Vec3i point4;
        point4 << 10.0, 10.0, 5.0;

        WHEN("Rasterize tetrahedron")
        {
            phantoms::Tetrahedron<double> tet{amplit, point1, point2, point3, point4};
            phantoms::rasterize<phantoms::Blending::ADDITION>(tet, dd, dc);

            IndexVector_t idx(3);

            WHEN("Check empty volume")
            {
                idx << 1, 3, 5;
                REQUIRE_EQ(dc(idx), 0);

                idx << 9, 5, 3;
                REQUIRE_EQ(dc(idx), 0);
            }
            WHEN("Check filled volume")
            {
                idx << 5, 5, 5;
                REQUIRE_EQ(dc(idx), amplit);
            }
        }
    }
}