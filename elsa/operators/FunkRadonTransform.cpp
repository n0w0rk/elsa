#include "FunkRadonTransform.h"
#include "Math.hpp"
#include "DataContainer.h"
#include "IdenticalBlocksDescriptor.h"

namespace elsa
{
    std::tuple<DataContainer<real_t>, DataContainer<real_t>, DataContainer<real_t>,
               DataContainer<real_t>, DataContainer<real_t>, DataContainer<real_t>>
        extractFiberDirection(const DataContainer<real_t>& sphField,
                              const axdt::SphericalFunctionInformation<real_t>& sfInfo,
                              const EdgeVecList& delauneyEdges, const index_t maxNumMaximaExtracted)
    {
        // initialisation of helper variables
        using MatrixXd_t = typename axdt::SphericalFieldsTransform<real_t>::MatrixXd_t;
        using DirVec = elsa::AXDTOperator<real_t>::DirVec;

        const axdt::SphericalFieldsTransform<real_t> spfTrans =
            axdt::SphericalFieldsTransform<real_t>(sfInfo);
        const FunkRadonTransform<real_t> frTrans =
            FunkRadonTransform<real_t>(sfInfo.symmetry, sfInfo.maxDegree);

        const MatrixXd_t funkTransformMatrix = frTrans.transformMatrix;
        const MatrixXd_t sphericalHarmonicsBasis = spfTrans.sphericalHarmonicsBasis;

        const IdenticalBlocksDescriptor* sphFieldDesc =
            &dynamic_cast<const IdenticalBlocksDescriptor&>(sphField.getDataDescriptor());
        const auto& volDesc = sphFieldDesc->getDescriptorOfBlock(0);

        auto mean = materialize(sphField.getBlock(0));
        // Generalized Fractional Anisotropy represents a quality measure
        // ( mentioned in the PhD of Matthias Wieczorek ) and is calculated based on the formula
        // provided in the PhD
        auto gfa = empty<real_t>(volDesc);

        // The magnitude (r) and direction (v) of the local maxima of the Funk-Radon space and edge
        // scattering for every voxel, which is the information that will be further used for
        // visualisations of microstructure directions.
        auto r = empty<real_t>(IdenticalBlocksDescriptor(maxNumMaximaExtracted, volDesc));
        auto rEdge = emptylike(r);
        auto v = empty<real_t>(IdenticalBlocksDescriptor(maxNumMaximaExtracted,
                                                         IdenticalBlocksDescriptor(3, volDesc)));
        auto vEdge = emptylike(v);

        const auto numEdges = static_cast<index_t>(delauneyEdges.size());
        const auto numDirs = static_cast<index_t>(sfInfo.dirs.size());
        const index_t volumeSize = volDesc.getNumberOfCoefficients();

        const VolumeDescriptor spfDesc = VolumeDescriptor({numDirs});
        const VolumeDescriptor sphDesc = VolumeDescriptor({sfInfo.basisCnt});

#pragma omp parallel for
        for (index_t idx = 0; idx < volumeSize; idx++) {
            auto spf = empty<real_t>(spfDesc);
            auto spfEdges = empty<real_t>(spfDesc);
            auto sph = empty<real_t>(sphDesc);

            Eigen::Map<MatrixXd_t> spfMap((thrust::raw_pointer_cast(spf.storage().data())), numDirs,
                                          1);
            Eigen::Map<MatrixXd_t> sphMap((thrust::raw_pointer_cast(sph.storage().data())),
                                          sfInfo.basisCnt, 1);

            // invert negative values
            for (index_t blk = 0; blk < sphFieldDesc->getNumberOfBlocks(); blk++) {
                sph[blk] = sphField[blk * volumeSize + idx];
            }

            // * ADJOINT TRANSFORM OF SPFTrans
            // * sphTrans->applyAdjoint(sph, spf);
            // * [numDirs, basisCnt] * [basisCnt, 1] = [numDirs, 1]
            spfMap = sphericalHarmonicsBasis * sphMap;

            spf = cwiseAbs(spf);
            spfEdges = spf;

            // * FORWARD TRANSFORM OF SPFTrans
            // * sphTrans->apply(spf, sph);
            // * [basisCnt, numDirs] * [numDirs, 1] = [basisCnt, 1]
            sphMap = sphericalHarmonicsBasis.transpose()
                     * (sfInfo.weights.array() * spfMap.array()).matrix();

            // store gfa
            gfa[idx] = std::sqrt(1 - sph[0] * sph[0] / sph.squaredL2Norm());

            // * FORWARD FUNK TRANSFORM
            // * apply Funk-Radon transform
            // * [basisCnt, basisCnt] * [basisCnt, 1] = [basisCnt, 1]
            sphMap = funkTransformMatrix * sphMap;

            // fiber extraction
            // * ADJOINT TRANSFORM OF SPFTrans
            // * _sphTrans->applyAdjoint(sph, spf);
            // * [numDirs, basisCnt] * [basisCnt, 1] = [numDirs, 1]
            spfMap = sphericalHarmonicsBasis * sphMap;

            // start with fiber extraction
            std::vector<index_t> localFiberMaxIndex;
            std::vector<index_t> localEdgeMaxIndex;
            std::vector<int> flagsFiber(numDirs, 0);
            std::vector<int> flagsEdge(numDirs, 0);

            // set flags on neighborhood relation
            for (index_t i = 0; i < numEdges; i++) {
                index_t index1 = delauneyEdges[i][0];
                index_t index2 = delauneyEdges[i][1];
                float valFiber1 = spf(index1);
                float valFiber2 = spf(index2);
                float valEdge1 = spfEdges(index1);
                float valEdge2 = spfEdges(index2);

                // consider fibers
                if (valFiber1 < valFiber2) {
                    flagsFiber[index1] = -1;
                    if (!flagsFiber[index2])
                        flagsFiber[index2] = 1;
                } else if (valFiber1 > valFiber2) {
                    flagsFiber[index2] = -1;
                    if (!flagsFiber[index1])
                        flagsFiber[index1] = 1;
                }

                // consider edge scattering
                if (valEdge1 < valEdge2) {
                    flagsEdge[index1] = -1;
                    if (!flagsEdge[index2])
                        flagsEdge[index2] = 1;
                } else if (valEdge1 > valEdge2) {
                    flagsEdge[index2] = -1;
                    if (!flagsEdge[index1])
                        flagsEdge[index1] = 1;
                }
            }

            // add all local maxima to the list localMaxIndex
            auto it = std::find(flagsFiber.begin(), flagsFiber.end(), 1);
            while (it != flagsFiber.end()) {
                localFiberMaxIndex.push_back(std::distance(flagsFiber.begin(), it));
                it = std::find(std::next(it), flagsFiber.end(), 1);
            }
            it = std::find(flagsEdge.begin(), flagsEdge.end(), 1);
            while (it != flagsEdge.end()) {
                localEdgeMaxIndex.push_back(std::distance(flagsEdge.begin(), it));
                it = std::find(std::next(it), flagsEdge.end(), 1);
            }

            // get sorted indices by magnitude
            std::sort(localFiberMaxIndex.begin(), localFiberMaxIndex.end(),
                      [&spf](index_t a, index_t b) { return spf(a) > spf(b); });

            std::sort(localEdgeMaxIndex.begin(), localEdgeMaxIndex.end(),
                      [&spfEdges](index_t a, index_t b) { return spfEdges(a) > spfEdges(b); });

            // we discard any vertices that is closer than 10deg to another point which was already
            // added
            float cosSim = std::cos(pi<real_t> / 180 * 10);

            // remove duplicates
            std::vector<index_t> localFiberMaxUniqueIndex;
            std::vector<index_t> localEdgeMaxUniqueIndex;

            for (index_t i : localFiberMaxIndex) {
                DirVec vec1 = sfInfo.dirs[i];

                const auto listSize = static_cast<index_t>(localFiberMaxUniqueIndex.size());
                index_t j = 0;
                for (; j < listSize; j++) {
                    DirVec vec2 = sfInfo.dirs[localFiberMaxUniqueIndex[j]];
                    if (vec1.dot(vec2) > cosSim)
                        break;
                }

                // if we iterated fully through localFiberMaxUniqueIndex it is a unique vector
                if (j == listSize)
                    localFiberMaxUniqueIndex.push_back(i);
            }

            for (index_t i : localEdgeMaxIndex) {
                DirVec vec1 = sfInfo.dirs[i];

                const auto listSize = static_cast<index_t>(localEdgeMaxUniqueIndex.size());
                index_t j = 0;
                for (; j < listSize; j++) {
                    DirVec vec2 = sfInfo.dirs[localEdgeMaxUniqueIndex[j]];
                    if (vec1.dot(vec2) > cosSim)
                        break;
                }

                // if we iterated fully through localEdgeMaxUniqueIndex it is a unique vector
                if (j == listSize)
                    localEdgeMaxUniqueIndex.push_back(i);
            }

            // store the localMax values in descending order up to the maxNumOf
            const index_t maxIndex = r.getDataDescriptor().getNumberOfCoefficientsPerDimension()[3];
            const auto localFiberMaxUniqueIndexSize =
                static_cast<index_t>(localFiberMaxUniqueIndex.size());
            const auto localEdgeMaxUniqueIndexSize =
                static_cast<index_t>(localEdgeMaxUniqueIndex.size());

            for (index_t i = 0; i < maxIndex; i++) {
                if (i < localFiberMaxUniqueIndexSize) {
                    r[i * volumeSize + idx] = spf(localFiberMaxUniqueIndex[i]);
                    DirVec dir = sfInfo.dirs[localFiberMaxUniqueIndex[i]];
                    v[i * 3 * volumeSize + idx] = dir[0];
                    v[i * 3 * volumeSize + volumeSize + idx] = dir[1];
                    v[i * 3 * volumeSize + 2 * volumeSize + idx] = dir[2];
                }
                if (i < localEdgeMaxUniqueIndexSize) {
                    rEdge[i * volumeSize + idx] = spfEdges(localEdgeMaxUniqueIndex[i]);
                    DirVec dir = sfInfo.dirs[localEdgeMaxUniqueIndex[i]];
                    vEdge[i * 3 * volumeSize + idx] = dir[0];
                    vEdge[i * 3 * volumeSize + volumeSize + idx] = dir[1];
                    vEdge[i * 3 * volumeSize + 2 * volumeSize + idx] = dir[2];
                }
            }
        }

        return std::make_tuple(r, v, rEdge, vEdge, mean, gfa);
    }

    template <typename data_t>
    Eigen::DiagonalMatrix<data_t, Eigen::Dynamic>
        funk_transform_matrix(const index_t& numberOfCoefficients,
                              const typename AXDTOperator<data_t>::Symmetry& symmetry)
    {
        using Symmetry = typename AXDTOperator<data_t>::Symmetry;

        Eigen::DiagonalMatrix<data_t, Eigen::Dynamic> transformMatrix(numberOfCoefficients);
        int l;
        int step;
        switch (symmetry) {
            case Symmetry::regular:
                l = 0;
                step = 1;
                break;
            case Symmetry::even:
                l = 0;
                step = 2;
                break;
            case Symmetry::odd:
                l = 1;
                step = 2;
                break;
            default:
                throw std::logic_error("math::axdt::funk_transform_matrix: Unrecognized "
                                       "spherical harmonics symmetry type");
        }

        for (index_t i = 0; i < numberOfCoefficients; l += step) {
            for (int m = -l; m <= l; m++, i++) {
                if (l % 2 == 0) {
                    if (l == 0) {
                        transformMatrix.diagonal()[i] = 1;
                    } else {
                        transformMatrix.diagonal()[i] = pow(-1, l / 2)
                                                        * axdt::double_factorial<data_t>(l - 1)
                                                        / axdt::double_factorial<data_t>(l);
                    }
                } else {
                    transformMatrix.diagonal()[i] = 0;
                }
            }
        }

        return transformMatrix;
    }
    template <typename data_t>
    FunkRadonTransform<data_t>::FunkRadonTransform(const Symmetry& symmetry,
                                                   const index_t& maxDegree)
        : transformMatrix(funk_transform_matrix<data_t>(
            axdt::SphericalFunctionInformation<data_t>::calculate_basis_cnt(symmetry, maxDegree),
            symmetry))
    {
    }
} // namespace elsa