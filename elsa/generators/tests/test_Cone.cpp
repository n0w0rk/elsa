/**
 * @file test_Cone.cpp
 *
 * @brief Test for Cone stump
 */

#include "VolumeDescriptor.h"
#include "doctest/doctest.h"

#include "PhantomDefines.h"
#include "Cone.h"

using namespace elsa;
using namespace doctest;

TEST_CASE("cone tests")
{

    GIVEN("A shifted cone stump along x axis")
    {

        index_t size = 10;
        IndexVector_t sizes(3);
        sizes << size, size, size;

        phantoms::Vec3i center;
        center << 5.0, 5.0, 5.0;

        double length{4.};
        double amplit{1.};
        double radius1{2.};
        double radius2{4.};

        VolumeDescriptor dd(sizes);
        DataContainer<double> dc(dd);
        dc = 0;

        WHEN("Rasterize cone stump")
        {

            phantoms::Cone<double> cone{
                elsa::phantoms::Orientation::X_AXIS, amplit, center, radius1, radius2, length};

            phantoms::rasterize<phantoms::Blending::ADDITION>(cone, dd, dc);

            IndexVector_t idx(3);

            WHEN("Check empty volume")
            {

                idx << 5, 2, 2;
                REQUIRE_EQ(dc(idx), 0);

                idx << 8, 5, 5;
                REQUIRE_EQ(dc(idx), 0);
            }
            WHEN("Check filled volume")
            {

                idx << 5, 5, 5;
                REQUIRE_EQ(dc(idx), amplit);

                idx << 5, 5, 6;
                REQUIRE_EQ(dc(idx), amplit);
            }
        }
    }

    GIVEN("A shifted cone stump along y axis")
    {

        index_t size = 10;
        IndexVector_t sizes(3);
        sizes << size, size, size;

        phantoms::Vec3i center;
        center << 5.0, 5.0, 5.0;

        double length{4.};
        double amplit{1.};
        double radius1{2.};
        double radius2{4.};

        VolumeDescriptor dd(sizes);
        DataContainer<double> dc(dd);
        dc = 0;

        WHEN("Rasterize cone stump")
        {

            phantoms::Cone<double> cone{
                elsa::phantoms::Orientation::Y_AXIS, amplit, center, radius1, radius2, length};

            phantoms::rasterize<phantoms::Blending::ADDITION>(cone, dd, dc);

            IndexVector_t idx(3);

            WHEN("Check empty volume")
            {

                idx << 2, 5, 2;
                REQUIRE_EQ(dc(idx), 0);

                idx << 5, 8, 5;
                REQUIRE_EQ(dc(idx), 0);
            }
            WHEN("Check filled volume")
            {

                idx << 5, 5, 5;
                REQUIRE_EQ(dc(idx), amplit);

                idx << 6, 5, 5;
                REQUIRE_EQ(dc(idx), amplit);
            }
        }
    }

    GIVEN("A shifted cone stump along z axis")
    {

        index_t size = 10;
        IndexVector_t sizes(3);
        sizes << size, size, size;

        phantoms::Vec3i center;
        center << 5.0, 5.0, 5.0;

        double length{4.};
        double amplit{1.};
        double radius1{2.};
        double radius2{4.};

        VolumeDescriptor dd(sizes);
        DataContainer<double> dc(dd);
        dc = 0;

        WHEN("Rasterize cone stump")
        {

            phantoms::Cone<double> cone{
                elsa::phantoms::Orientation::Z_AXIS, amplit, center, radius1, radius2, length};

            phantoms::rasterize<phantoms::Blending::ADDITION>(cone, dd, dc);

            IndexVector_t idx(3);

            WHEN("Check empty volume")
            {

                idx << 2, 2, 5;
                REQUIRE_EQ(dc(idx), 0);

                idx << 5, 5, 8;
                REQUIRE_EQ(dc(idx), 0);
            }
            WHEN("Check filled volume")
            {

                idx << 5, 5, 5;
                REQUIRE_EQ(dc(idx), amplit);

                idx << 5, 6, 5;
                REQUIRE_EQ(dc(idx), amplit);
            }
        }
    }
}