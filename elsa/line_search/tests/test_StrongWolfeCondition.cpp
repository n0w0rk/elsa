#include "doctest/doctest.h"

#include "StrongWolfeCondition.h"
#include "Identity.h"
#include "Logger.h"
#include "Scaling.h"
#include "VolumeDescriptor.h"
#include "LeastSquares.h"

using namespace elsa;
using namespace doctest;

TEST_SUITE_BEGIN("line_search");
TYPE_TO_STRING(StrongWolfeCondition<float>);
TYPE_TO_STRING(StrongWolfeCondition<double>);

template <template <typename> typename T, typename data_t>
constexpr data_t return_data_t(const T<data_t>&);

TEST_CASE_TEMPLATE("StrongWolfeCondition: Solving a simple problem", data_t, float, double)
{
    // Set seed for Eigen Matrices!
    srand((unsigned int) 666);

    // eliminate the timing info from console for the tests
    Logger::setLevel(Logger::LogLevel::OFF);

    GIVEN("a simple problem")
    {
        IndexVector_t numCoeff(2);
        numCoeff << 2, 2;
        VolumeDescriptor dd(numCoeff);

        Eigen::Matrix<data_t, -1, 1> bVec(dd.getNumberOfCoefficients());
        bVec.setRandom();
        DataContainer dcB(dd, bVec);

        Vector_t<data_t> randomData(dd.getNumberOfCoefficients());
        randomData.setRandom();
        DataContainer<data_t> scaleFactors(dd, randomData * static_cast<data_t>(5.14));

        Scaling<data_t> scalingOp(dd, scaleFactors);
        LeastSquares<data_t> prob(scalingOp, dcB);

        WHEN("setting up a StrongWolfeCondition Line Search and 0s for an initial guess")
        {
            StrongWolfeCondition<data_t> line_search{prob, 10, 1e-4, 0.9};

            THEN("the clone works correctly")
            {
                auto lsClone = line_search.clone();

                CHECK_NE(lsClone.get(), &line_search);
                CHECK_EQ(*lsClone, line_search);

                AND_THEN("it works as expected")
                {
                    auto xi = DataContainer<data_t>(prob.getDomainDescriptor());
                    xi = 0;
                    std::array<data_t, 10> solutions;
                    if (std::string("d") == typeid(data_t).name()) {
#ifdef ELSA_HAS_CUDA_PROJECTORS
                        solutions = {
                            0.08333333333333332870740406406184774823486804962158203125,
                            0.08333333333333332870740406406184774823486804962158203125,
                            0.08333333333333332870740406406184774823486804962158203125,
                            0.08333333333333332870740406406184774823486804962158203125,
                            0.041666666666666664353702032030923874117434024810791015625,
                            0.08333333333333332870740406406184774823486804962158203125,
                            0.08333333333333332870740406406184774823486804962158203125,
                            0.111111111111111104943205418749130330979824066162109375,
                            0.125,
                            0.0544871794871794878911686055289465002715587615966796875,
                        };
#else
                        solutions = {
                            0.08333333333333332870740406406184774823486804962158203125,
                            0.08333333333333332870740406406184774823486804962158203125,
                            0.08333333333333332870740406406184774823486804962158203125,
                            0.08333333333333332870740406406184774823486804962158203125,
                            0.0625,
                            0.08333333333333332870740406406184774823486804962158203125,
                            0.048611111111111111882099322656358708627521991729736328125,
                            0.5,
                            0.041666666666666664353702032030923874117434024810791015625,
                            0.5,
                        };
#endif // ELSA_HAS_CUDA_PROJECTORS
                    } else if (std::string("f") == typeid(data_t).name()) {
#ifdef ELSA_HAS_CUDA_PROJECTORS
                        solutions = {
                            0.0416666679084300994873046875,
                            0.0500000007450580596923828125,
                            1.0,
                            0.0416666679084300994873046875,
                            0.20833332836627960205078125,
                            0.083333335816860198974609375,
                            0.066666670143604278564453125,
                            0.125,
                            0.083333335816860198974609375,
                            0.083333335816860198974609375,
                        };
#else
                        solutions = {
                            0.0416666679084300994873046875,
                            0.0500000007450580596923828125,
                            1,
                            0.0416666679084300994873046875,
                            0.20833332836627960205078125,
                            0.0548696853220462799072265625,
                            0.5,
                            0.0416666679084300994873046875,
                            0.5,
                            0.083333335816860198974609375,
                        };
#endif // ELSA_HAS_CUDA_PROJECTORS
                    }
                    for (index_t i = 0; i < solutions.size(); ++i) {
                        auto di = -prob.getGradient(xi);
                        data_t alpha = line_search.solve(xi, di);
                        CHECK_EQ(alpha - solutions[i], doctest::Approx(0));
                        xi += alpha * di;
                    }
                }
            }
        }

        WHEN("setting up StrongWolfeCondition line search and 1s for an intial guess")
        {
            StrongWolfeCondition<data_t> line_search{prob, 10, 1e-4, 0.9};

            THEN("the clone works correctly")
            {
                auto lsClone = line_search.clone();

                CHECK_NE(lsClone.get(), &line_search);
                CHECK_EQ(*lsClone, line_search);

                AND_THEN("it works as expected")
                {
                    auto xi = DataContainer<data_t>(prob.getDomainDescriptor());
                    xi = 1;
                    std::array<data_t, 10> solutions;
                    if (std::string("d") == typeid(data_t).name()) {
#ifdef ELSA_HAS_CUDA_PROJECTORS
                        solutions = {
                            0.08333333333333332870740406406184774823486804962158203125,
                            0.08333333333333332870740406406184774823486804962158203125,
                            0.041666666666666664353702032030923874117434024810791015625,
                            0.04938271604938272718587910503629245795309543609619140625,
                            1.0,
                            0.08333333333333332870740406406184774823486804962158203125,
                            0.0625,
                            0.08333333333333332870740406406184774823486804962158203125,
                            0.0625,
                            0.08333333333333332870740406406184774823486804962158203125,
                        };
#else
                        solutions = {
                            0.08333333333333332870740406406184774823486804962158203125,
                            0.08333333333333332870740406406184774823486804962158203125,
                            0.08333333333333332870740406406184774823486804962158203125,
                            0.08333333333333332870740406406184774823486804962158203125,
                            0.041666666666666664353702032030923874117434024810791015625,
                            0.0555555555555555524716027093745651654899120330810546875,
                            0.25,
                            0.0625,
                            0.08333333333333332870740406406184774823486804962158203125,
                            0.08333333333333332870740406406184774823486804962158203125,
                        };
#endif // ELSA_HAS_CUDA_PROJECTORS
                    } else if (std::string("f") == typeid(data_t).name()) {
#ifdef ELSA_HAS_CUDA_PROJECTORS
                        solutions = {
                            0.0625,
                            0.0625,
                            0.0625,
                            0.098765432834625244140625,
                            0.083333335816860198974609375,
                            0.0493827164173126220703125,
                            1.0,
                            0.0625,
                            0.0416666679084300994873046875,
                            0.104166664183139801025390625,
                        };
#else
                        solutions = {
                            0.0625,
                            0.0625,
                            0.0625,
                            0.0493827164173126220703125,
                            1,
                            0.0625,
                            0.0493827164173126220703125,
                            0.125,
                            0.083333335816860198974609375,
                            0.083333335816860198974609375,
                        };
#endif // ELSA_HAS_CUDA_PROJECTORS
                    }
                    for (index_t i = 0; i < solutions.size(); ++i) {
                        auto di = -prob.getGradient(xi);
                        data_t alpha = line_search.solve(xi, di);
                        CHECK_EQ(alpha - solutions[i], doctest::Approx(0));
                        xi += alpha * di;
                    }
                }
            }
        }
    }
}
TEST_SUITE_END();
