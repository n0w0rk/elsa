#include "Cone.h"
#include "DataContainer.h"
#include "PhantomDefines.h"
#include "VolumeDescriptor.h"
#include "elsaDefines.h"
#include <csignal>
#include "Logger.h"

namespace elsa::phantoms
{

    template <typename data_t>
    Cone<data_t>::Cone(Orientation o, data_t amplit, elsa::phantoms::Vec3i center, data_t radius1,
                       data_t radius2, data_t length)
        : _orientation{o},
          _amplit{amplit},
          _center{center},
          _radius1{radius1},
          _radius2{radius2},
          _length{length}
    {
        _start = std::lround(_center[INDEX_X] - _length / 2.0l);
        if (o == Orientation::Y_AXIS) {
            _start = std::lround(_center[INDEX_Y] - _length / 2.0l);
        } else if (o == Orientation::Z_AXIS) {
            _start = std::lround(_center[INDEX_Z] - _length / 2.0l);
        }
        rSlope = (radius2 - radius1) / length;
    };

    template <typename data_t>
    bool Cone<data_t>::isInCone(const Vec3i& idx) const
    {
        if (_orientation == Orientation::X_AXIS) {
            if (idx[INDEX_X] < 0 || idx[INDEX_X] >= _length) {
                return false;
            }
            return (double(idx[INDEX_Y] * idx[INDEX_Y]) + double(idx[INDEX_Z] * idx[INDEX_Z]))
                   <= (double(_radius1) + double(rSlope * idx[INDEX_X]))
                          * (double(_radius1) + double(rSlope * idx[INDEX_X]));
        } else if (_orientation == Orientation::Y_AXIS) {
            if (idx[INDEX_Y] < 0 || idx[INDEX_Y] >= _length) {
                return false;
            }
            return (double(idx[INDEX_X] * idx[INDEX_X]) + double(idx[INDEX_Z] * idx[INDEX_Z]))
                   <= (double(_radius1) + double(rSlope * idx[INDEX_Y]))
                          * (double(_radius1) + double(rSlope * idx[INDEX_Y]));
        } else {
            if (idx[INDEX_Z] < 0 || idx[INDEX_Z] >= _length) {
                return false;
            }
            return (double(idx[INDEX_X] * idx[INDEX_X]) + double(idx[INDEX_Y] * idx[INDEX_Y]))
                   <= (double(_radius1) + double(rSlope * idx[INDEX_Z]))
                          * (double(_radius1) + double(rSlope * idx[INDEX_Z]));
        }
    }

    template <Blending b, typename data_t>
    void rasterize(Cone<data_t>& cone, VolumeDescriptor& dd, DataContainer<data_t>& dc)
    {

        Vec3i idx(3);
        idx << 0, 0, 0;

        auto strides = dd.getProductOfCoefficientsPerDimension();

        index_t aOffset = 0;
        index_t bOffset = 0;

        index_t aOffsetNeg = 0;
        index_t bOffsetNeg = 0;

        index_t cOffset = 0;

        Vec3i _center = cone.getCenter();
        data_t _amplit = cone.getAmplitude();

        data_t _start = cone.getStart();

        int TEMP_A = INDEX_X;
        int TEMP_B = INDEX_Y;
        int TEMP_C = INDEX_Z;

        if (cone.getOrientation() == Orientation::X_AXIS) {
            TEMP_A = INDEX_Y;
            TEMP_B = INDEX_Z;
            TEMP_C = INDEX_X;

        } else if (cone.getOrientation() == Orientation::Y_AXIS) {
            TEMP_A = INDEX_X;
            TEMP_B = INDEX_Z;
            TEMP_C = INDEX_Y;
        }

        // Slice over Axis of orientation
        for (; cone.isInCone(idx); ++idx[TEMP_C]) {

            cOffset = (idx[TEMP_C] + _start) * strides[TEMP_C];

            for (; cone.isInCone(idx); ++idx[TEMP_B]) {

                bOffset = (idx[TEMP_B] + _center[TEMP_B]) * strides[TEMP_B];
                bOffsetNeg = (-idx[TEMP_B] + _center[TEMP_B]) * strides[TEMP_B];

                for (; cone.isInCone(idx); ++idx[TEMP_A]) {

                    aOffset = (idx[TEMP_A] + _center[TEMP_A]) * strides[TEMP_A];
                    aOffsetNeg = (-idx[TEMP_A] + _center[TEMP_A]) * strides[TEMP_A];

                    Logger::get("Cone::rasterize")
                        ->info("indices: {},{},{}\ndc Index {}", idx[TEMP_C] + _start,
                               idx[TEMP_B] + _center[TEMP_B], idx[TEMP_A] + _center[TEMP_A],
                               aOffset + bOffset + cOffset);
                    Logger::get("Cone::rasterize")
                        ->info("offsets {},{},{}", cOffset, bOffset, aOffset);

                    blend<b>(dc, aOffset + bOffset + cOffset, _amplit);

                    if (idx[TEMP_A] != 0) {
                        blend<b>(dc, aOffsetNeg + bOffset + cOffset, _amplit);
                    }

                    if (idx[TEMP_B] != 0) {
                        blend<b>(dc, aOffset + bOffsetNeg + cOffset, _amplit);
                    }

                    if (idx[TEMP_A] != 0 && idx[TEMP_B] != 0) {
                        blend<b>(dc, aOffsetNeg + bOffsetNeg + cOffset, _amplit);
                    }
                }
                idx[TEMP_A] = 0;
            }
            idx[TEMP_B] = 0;
        }
        Logger::get("Cone::rasterize")
            ->info("factors: {},{},{}", strides[TEMP_C], strides[TEMP_B], strides[TEMP_A]);
    }

    template class Cone<float>;
    template class Cone<double>;

    template void rasterize<Blending::ADDITION, float>(Cone<float>&, VolumeDescriptor&,
                                                       DataContainer<float>&);
    template void rasterize<Blending::ADDITION, double>(Cone<double>&, VolumeDescriptor&,
                                                        DataContainer<double>&);
    template void rasterize<Blending::OVERWRITE, float>(Cone<float>&, VolumeDescriptor&,
                                                        DataContainer<float>&);
    template void rasterize<Blending::OVERWRITE, double>(Cone<double>&, VolumeDescriptor&,
                                                         DataContainer<double>&);
} // namespace elsa::phantoms
