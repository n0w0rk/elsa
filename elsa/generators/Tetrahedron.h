#pragma once
#include "PhantomDefines.h"

namespace elsa::phantoms
{
    template <typename data_t = double>
    class Tetrahedron
    {

    private:
        data_t _amplit;
        Vec3i _point1;
        Vec3i _point2;
        Vec3i _point3;
        Vec3i _point4;

        Vec3X<data_t> _p1X;
        Vec3X<data_t> _p2X;
        Vec3X<data_t> _p3X;
        Vec3X<data_t> _p4X;

    public:
        /**
         * Creates a tetrahedron with vertices at the given points.
         *
         * @param amlpit amplitude wich is added to the voxel on rasterization
         * @param point1 first point of the tetrahedron
         * @param point2 second point of the tetrahedron
         * @param point3 third point of the tetrahedron
         * @param point4 fourth point of the tetrahedron
         */
        Tetrahedron(data_t amplit, Vec3i point1, Vec3i point2, Vec3i point3, Vec3i point4);

        /**
         * @brief returns the amplitude to color the voxel
         */
        const data_t getAmplitude() const { return _amplit; };
        /**
         * @brief returns the first point of the tetrahedron
         */
        const Vec3i& getPoint1() const { return _point1; };
        /**
         * @brief returns the second point of the tetrahedron
         */
        const Vec3i& getPoint2() const { return _point2; };
        /**
         * @brief returns the third point of the tetrahedron
         */
        const Vec3i& getPoint3() const { return _point3; };
        /**
         * @brief returns the fourth point of the tetrahedron
         */
        const Vec3i& getPoint4() const { return _point4; };

        bool isInTetrahedron(const Vec3X<data_t>& idx) const;
    };

    /**
     * @brief rasterizes a tetrahedron in the given data container
     */
    template <Blending b, typename data_t>
    void rasterize(Tetrahedron<data_t>& tet, VolumeDescriptor& dd, DataContainer<data_t>& dc);

} // namespace elsa::phantoms

/**
 * @brief Tetrahedron formatter to use the Logger.h functions
 */
template <typename data_t>
struct fmt::formatter<elsa::phantoms::Tetrahedron<data_t>> : fmt::formatter<std::string> {
    auto format(elsa::phantoms::Tetrahedron<data_t> tet, format_context& ctx) -> decltype(ctx.out())
    {
        auto _amplit = tet.getAmplitude();
        auto _point1 = tet.getPoint1();
        auto _point2 = tet.getPoint2();
        auto _point3 = tet.getPoint3();
        auto _point4 = tet.getPoint4();

        return format_to(ctx.out(),
                         "Tetrahedron: amplitude: {}, point1: ({},{},{}), "
                         "point2: ({},{},{}), point3: ({},{},{}), point4: ({},{},{})",
                         _amplit, _point1[elsa::phantoms::INDEX_X],
                         _point1[elsa::phantoms::INDEX_Y], _point1[elsa::phantoms::INDEX_Z],
                         _point2[elsa::phantoms::INDEX_X], _point2[elsa::phantoms::INDEX_Y],
                         _point2[elsa::phantoms::INDEX_Z], _point3[elsa::phantoms::INDEX_X],
                         _point3[elsa::phantoms::INDEX_Y], _point3[elsa::phantoms::INDEX_Z],
                         _point4[elsa::phantoms::INDEX_X], _point4[elsa::phantoms::INDEX_Y],
                         _point4[elsa::phantoms::INDEX_Z]);
    }
};
