#include "Tetrahedron.h"
#include "BoundingBox.h"
#include "PhantomDefines.h"
#include "elsaDefines.h"
#include <array>

namespace elsa::phantoms
{

    template <typename data_t>
    Tetrahedron<data_t>::Tetrahedron(data_t amplit, Vec3i point1, Vec3i point2, Vec3i point3,
                                     Vec3i point4)
        : _amplit{amplit}, _point1{point1}, _point2{point2}, _point3{point3}, _point4{point4}
    {

        _p1X = _point1.cast<data_t>();
        _p2X = _point2.cast<data_t>();
        _p3X = _point3.cast<data_t>();
        _p4X = _point4.cast<data_t>();
    };

    template <typename data_t>
    bool sameSideOfTri(const Vec3X<data_t> idx, const Vec3X<data_t>& p, const Vec3X<data_t>& p1,
                       const Vec3X<data_t>& p2, const Vec3X<data_t>& p3)
    {

        Vec3X<data_t> normal = (p2 - p1).cross(p3 - p1);
        data_t dp = normal.dot(p - p1);
        data_t didx = normal.dot(idx - p1);

        return (dp >= 0 && didx >= 0) || (dp < 0 && didx < 0);
    };

    template <typename data_t>
    bool Tetrahedron<data_t>::isInTetrahedron(const Vec3X<data_t>& idx) const
    {

        // Check if idx is in the tetrahedron

        return sameSideOfTri<data_t>(idx, _p1X, _p2X, _p3X, _p4X)
               && sameSideOfTri<data_t>(idx, _p2X, _p1X, _p3X, _p4X)
               && sameSideOfTri<data_t>(idx, _p3X, _p1X, _p2X, _p4X)
               && sameSideOfTri<data_t>(idx, _p4X, _p1X, _p2X, _p3X);
    };

    template <typename data_t>
    std::array<data_t, 6> boundingBox(const Vec3i& _point1, const Vec3i& _point2,
                                      const Vec3i& _point3, const Vec3i& _point4)
    {

        index_t minX, minY, minZ, maxX, maxY, maxZ;

        minX = std::min({_point1[INDEX_X], _point2[INDEX_X], _point3[INDEX_X], _point4[INDEX_X]});
        minY = std::min({_point1[INDEX_Y], _point2[INDEX_Y], _point3[INDEX_Y], _point4[INDEX_Y]});
        minZ = std::min({_point1[INDEX_Z], _point2[INDEX_Z], _point3[INDEX_Z], _point4[INDEX_Z]});

        maxX = std::max({_point1[INDEX_X], _point2[INDEX_X], _point3[INDEX_X], _point4[INDEX_X]});
        maxY = std::max({_point1[INDEX_Y], _point2[INDEX_Y], _point3[INDEX_Y], _point4[INDEX_Y]});
        maxZ = std::max({_point1[INDEX_Z], _point2[INDEX_Z], _point3[INDEX_Z], _point4[INDEX_Z]});

        return {minX, minY, minZ, maxX, maxY, maxZ};
    }

    template <Blending b, typename data_t>
    void rasterize(Tetrahedron<data_t>& tet, VolumeDescriptor& dd, DataContainer<data_t>& dc)
    {
        auto [minX, minY, minZ, maxX, maxY, maxZ] =
            boundingBox<data_t>(tet.getPoint1(), tet.getPoint2(), tet.getPoint3(), tet.getPoint4());

        Vec3X<data_t> idx(3);
        idx << minX, minY, minZ;

        index_t zOffset = 0;
        index_t yOffset = 0;
        index_t xOffset = 0;

        data_t _amplit = tet.getAmplitude();

        auto strides = dd.getProductOfCoefficientsPerDimension();

        for (index_t z = index_t(minZ); z <= index_t(maxZ); z++, idx[INDEX_Z]++) {
            zOffset = z * strides[INDEX_Z];

            for (index_t y = index_t(minY); y <= index_t(maxY); y++, idx[INDEX_Y]++) {
                yOffset = y * strides[INDEX_Y];

                for (index_t x = index_t(minX); x <= index_t(maxX); x++, idx[INDEX_X]++) {
                    xOffset = x * strides[INDEX_X];

                    if (tet.isInTetrahedron(idx)) {
                        blend<b>(dc, zOffset + yOffset + xOffset, _amplit);
                    }
                }
                idx[INDEX_X] = minX;
            }
            idx[INDEX_Y] = minY;
        }
    };

    template class Tetrahedron<float>;
    template class Tetrahedron<double>;

    template void rasterize<Blending::ADDITION, float>(Tetrahedron<float>& tet,
                                                       VolumeDescriptor& dd,
                                                       DataContainer<float>& dc);
    template void rasterize<Blending::ADDITION, double>(Tetrahedron<double>& tet,
                                                        VolumeDescriptor& dd,
                                                        DataContainer<double>& dc);

    template void rasterize<Blending::OVERWRITE, float>(Tetrahedron<float>& tet,
                                                        VolumeDescriptor& dd,
                                                        DataContainer<float>& dc);
    template void rasterize<Blending::OVERWRITE, double>(Tetrahedron<double>& tet,
                                                         VolumeDescriptor& dd,
                                                         DataContainer<double>& dc);

} // namespace elsa::phantoms