#include "ConeFree.h"
#include "Logger.h"
#include "PhantomDefines.h"
#include "elsaDefines.h"

namespace elsa::phantoms
{

    template <typename data_t>
    ConeFree<data_t>::ConeFree(data_t amplit, Vec3i center, data_t radius1, data_t radius2,
                               data_t length, Vec3X<data_t> eulers)
        : _amplit{amplit},
          _center{center},
          _radius1{radius1},
          _radius2{radius2},
          _length{length},
          _eulers{eulers}
    {
        _centerX = _center.template cast<data_t>();

        fillRotationMatrix(_eulers, rot);
        rot.transposeInPlace();
    };

    template <typename data_t>
    bool ConeFree<data_t>::isInConeFree(const Vec3X<data_t>& idx, data_t halfLength) const
    {

        Vec3X<data_t> shifted{idx - _centerX};
        Vec3X<data_t> rotated{getInvRotationMatrix() * shifted};

        if (std::abs(rotated[INDEX_Z]) > halfLength) {
            return false;
        } else {
            return (double(rotated[INDEX_X] * rotated[INDEX_X])
                    + double(rotated[INDEX_Y] * rotated[INDEX_Y]))
                   <= (double(_radius1)
                       + double((_radius2 - _radius1) / _length * (rotated[INDEX_Z] + halfLength)))
                          * (double(_radius1)
                             + double((_radius2 - _radius1) / _length
                                      * (rotated[INDEX_Z] + halfLength)));
        }
    };

    template <typename data_t>
    index_t getHalfDiagonal(ConeFree<data_t>& cone, const index_t centerHalfLength)
    {
        auto maxRadius = std::max(cone.getRadius1(), cone.getRadius2());
        // Diagonal from center to edge of radius on the end of the cone with theorem of
        // pythagoras
        return std::ceil(
            std::sqrt(double(centerHalfLength) * double(centerHalfLength) + maxRadius * maxRadius));
    };

    template <typename data_t>
    std::array<data_t, 6>
        boundingBox(const data_t halfLength, const Vec3i& _center,
                    const Vec3i& ncpd /*numberOfCoefficientsPerDimension zero based*/)
    {

        data_t minX, minY, minZ, maxX, maxY, maxZ;

        minX = _center[INDEX_X] - halfLength > 0 ? _center[INDEX_X] - halfLength : 0;
        minY = _center[INDEX_Y] - halfLength > 0 ? _center[INDEX_Y] - halfLength : 0;
        minZ = _center[INDEX_Z] - halfLength > 0 ? _center[INDEX_Z] - halfLength : 0;

        maxX = _center[INDEX_X] + halfLength < ncpd[INDEX_X] ? _center[INDEX_X] + halfLength
                                                             : ncpd[INDEX_X] - 1;
        maxY = _center[INDEX_Y] + halfLength < ncpd[INDEX_Y] ? _center[INDEX_Y] + halfLength
                                                             : ncpd[INDEX_Y] - 1;
        maxZ = _center[INDEX_Z] + halfLength < ncpd[INDEX_Z] ? _center[INDEX_Z] + halfLength
                                                             : ncpd[INDEX_Z] - 1;

        return {minX, minY, minZ, maxX, maxY, maxZ};
    };

    template <Blending b, typename data_t>
    void rasterize(ConeFree<data_t>& cone, VolumeDescriptor& dd, DataContainer<data_t>& dc)
    {

        auto strides = dd.getProductOfCoefficientsPerDimension();

        index_t xOffset = 0;
        index_t yOffset = 0;
        index_t zOffset = 0;

        Vec3i _center = cone.getCenter();
        data_t _amplit = cone.getAmplitude();

        index_t halfLength = std::ceil(cone.getLength() / 2.0l);

        auto [minX, minY, minZ, maxX, maxY, maxZ] = boundingBox<data_t>(
            getHalfDiagonal(cone, halfLength), _center, dd.getNumberOfCoefficientsPerDimension());

        Vec3X<data_t> idx(3);
        idx << minX, minY, minZ;

        for (index_t z = index_t(minZ); z <= index_t(maxZ); z++, idx[INDEX_Z]++) {
            zOffset = z * strides[INDEX_Z];

            for (index_t y = index_t(minY); y <= index_t(maxY); y++, idx[INDEX_Y]++) {
                yOffset = y * strides[INDEX_Y];

                for (index_t x = index_t(minX); x <= index_t(maxX); x++, idx[INDEX_X]++) {
                    xOffset = x * strides[INDEX_X];

                    if (cone.isInConeFree(idx, halfLength)) {
                        blend<b>(dc, zOffset + yOffset + xOffset, _amplit);
                    }
                }
                idx[INDEX_X] = minX;
            }
            idx[INDEX_Y] = minY;
        }
    };

    template class ConeFree<float>;
    template class ConeFree<double>;

    template void rasterize<Blending::ADDITION, float>(ConeFree<float>& cone, VolumeDescriptor& dd,
                                                       DataContainer<float>& dc);
    template void rasterize<Blending::ADDITION, double>(ConeFree<double>& cone,
                                                        VolumeDescriptor& dd,
                                                        DataContainer<double>& dc);
    template void rasterize<Blending::OVERWRITE, float>(ConeFree<float>& cone, VolumeDescriptor& dd,
                                                        DataContainer<float>& dc);
    template void rasterize<Blending::OVERWRITE, double>(ConeFree<double>& cone,
                                                         VolumeDescriptor& dd,
                                                         DataContainer<double>& dc);
} // namespace elsa::phantoms