#include "elsaDefines.h"
#include "LinearOperator.h"
#include "AXDTOperator.h"

namespace elsa
{
    /**
     * @brief Function for extraction of fiber directions from AXDT reconstructions
     *
     * This function takes input DataContainers representing the Spherical Harmonics field,
     * Delauney edges, and Spherical Functions Information. It performs a series of
     * transformations and computations to extract fiber directions, local maxima, and other related
     * information.
     *
     * The results, including magnitude (r) and direction (v) of local maxima for Funk-Radon space
     * of fiber and edge scattering, mean value, and Generalized Fractional Anisotropy (gfa), are
     * returned as a tuple.
     *
     * @param sphField The Spherical Harmonics field data container.
     * @param sfInfo Information about Spherical Functions.
     * @param delauneyEdges Delauney edges data container.
     * @param maxNumMaximaExtracted Maximum number of local maxima to extract.
     *
     * @return A tuple containing DataContainers for r, v, rEdge, vEdge, mean, and gfa.
     *         The elements represent magnitude, direction, and other information of local maxima.
     *
     * @author Nikola Dinev (nikola.dinev@tum.de) - original implementation
     * @author Aleksandar Rončević (aleksandar.roncevic@tum.de) - port to elsa
     *
     */

    using EdgeVec = Eigen::Matrix<index_t, 2, 1>;
    using EdgeVecList = std::vector<EdgeVec>;
    std::tuple<DataContainer<real_t>, DataContainer<real_t>, DataContainer<real_t>,
               DataContainer<real_t>, DataContainer<real_t>, DataContainer<real_t>>
        extractFiberDirection(const DataContainer<real_t>& sphField,
                              const axdt::SphericalFunctionInformation<real_t>& sfInfo,
                              const EdgeVecList& delauneyEdges, index_t maxNumMaximaExtracted = 4);

    /**
     * @brief Helper method for generating the matrix that represents the Funk-Radon transform  the
     * spherical harmonics way.
     * The equation is defined in the Theorem 6.4 (p.54) in the PhD of Matthias Wieczorek
     *
     * @param numberOfCoefficients Number of the reconstructed spherical harmonics coefficients
     * @param symmetry Spherical Harmonics symmetry type
     * @return Diagonal matrix representing the transformation
     */
    template <typename data_t>
    Eigen::DiagonalMatrix<data_t, Eigen::Dynamic>
        funk_transform_matrix(const index_t& numberOfCoefficients,
                              const typename AXDTOperator<data_t>::Symmetry& symmetry);

    /**
     * @brief Funk-Radon transform for function represented by spherical harmonics
     *
     * @author Nikola Dinev (nikola.dinev@tum.de) - original implementation
     * @author Aleksandar Rončević (aleksandar.roncevic@tum.de) - port to elsa
     *
     * @tparam data_t real type
     */
    template <typename data_t = real_t>
    struct FunkRadonTransform {

        using MatrixXd_t = typename axdt::SphericalFieldsTransform<data_t>::MatrixXd_t;
        using Symmetry = typename AXDTOperator<data_t>::Symmetry;

        explicit FunkRadonTransform(const Symmetry& symmetry, const index_t& maxDegree);

        MatrixXd_t transformMatrix;
    };
} // namespace elsa