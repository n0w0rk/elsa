#pragma once
#include "PhantomDefines.h"
#include "Cone.h"

namespace elsa::phantoms
{

    template <typename data_t = double>
    class ConeFree
    {

    private:
        data_t _amplit;
        Vec3i _center;

        data_t _radius1;
        data_t _radius2;
        data_t _length;

        Vec3X<data_t> _eulers;

        // rotation matrix
        Eigen::Matrix<data_t, 3, 3> rot;

        /* Same as _center but with another typ for rotation calculation*/
        Vec3X<data_t> _centerX;

    public:
        /**
         * Creates a Cone stump. This creates Cones along arbitrary axis defined by the euler
         * angles. End radii are defined by radius1 and radius2. The length is the distance from one
         * end to the other.
         *
         * @param amlpit amplitude wich is added to the voxel on rasterization
         * @param center center of the object
         * @param radius1 radius of one cone end
         * @param radius2 radius of the other cone end
         * @param length the length from one side throught the center to the other side
         * @param eulers the euler angles for the rotation
         */
        ConeFree(data_t amplit, Vec3i center, data_t radius1, data_t radius2, data_t length,
                 Vec3X<data_t> eulers);
        /**
         * @brief returns the amplitude to color the voxel
         */
        const data_t getAmplitude() const { return _amplit; };
        /**
         * @brief returns the center of the ConeFree
         */
        const Vec3i& getCenter() const { return _center; };
        /**
         * @brief returns the radius of one end of the ConeFree
         */
        const data_t getRadius1() const { return _radius1; };
        /**
         * @brief returns the radius of the other end of the ConeFree
         */
        const data_t getRadius2() const { return _radius2; };
        /**
         * @brief returns the length of the ConeFree
         */
        const data_t getLength() const { return _length; };
        /**
         * @brief returns the euler angles for the rotation
         */
        const Vec3X<data_t>& getEulers() const { return _eulers; };
        /**
         * @brief gets inverse of the rotation matrix
         */
        const Eigen::Matrix<data_t, 3, 3>& getInvRotationMatrix() const { return rot; };

        bool isInConeFree(const Vec3X<data_t>& idx, data_t halfLength) const;
    };

    /**
     * @brief rasterize the ConeFree
     *
     * @tparam b Blending type
     * @tparam data_t data type
     * @param cone the ConeFree to rasterize
     * @param dd the VolumeDescriptor
     * @param dc the DataContainer
     */
    template <Blending b, typename data_t>
    void rasterize(ConeFree<data_t>& cone, VolumeDescriptor& dd, DataContainer<data_t>& dc);

} // namespace elsa::phantoms

/**
 * @brief ConeFree formatter to use the Logger.h functions
 */
template <typename data_t>
struct fmt::formatter<elsa::phantoms::ConeFree<data_t>> : fmt::formatter<std::string> {
    auto format(elsa::phantoms::ConeFree<data_t> cone, format_context& ctx) -> decltype(ctx.out())
    {
        auto _center = cone.getCenter();
        auto _amplit = cone.getAmplitude();
        auto _radius1 = cone.getRadius1();
        auto _radius2 = cone.getRadius2();
        auto _length = cone.getLength();
        auto _eulers = cone.getEulers();

        return format_to(ctx.out(),
                         "ConeFree with amplitude {}, Center ({},{},{}), radius1 {}, radius2 {}, "
                         "length {} with euler angels ({},{},{}) ",
                         _amplit, _center[elsa::phantoms::INDEX_X],
                         _center[elsa::phantoms::INDEX_Y], _center[elsa::phantoms::INDEX_Z],
                         _radius1, _radius2, _length, _eulers[elsa::phantoms::INDEX_A],
                         _eulers[elsa::phantoms::INDEX_B], _eulers[elsa::phantoms::INDEX_C]);
    }
};