#include <doctest/doctest.h>
#include <memory>

#include "DataContainer.h"
#include "Functional.h"
#include "L2Squared.h"
#include "testHelpers.h"
#include "MaskableSum.h"
#include "ConstantFunctional.h"
#include "VolumeDescriptor.h"

using namespace elsa;
using namespace doctest;

TEST_SUITE_BEGIN("functionals");

TEST_CASE_TEMPLATE("MaskableSum: Construction, Setter", data_t, float, complex<float>, double,
                   complex<double>)
{
    GIVEN("no functional")
    {
        std::vector<std::unique_ptr<Functional<data_t>>> empty;
        CHECK_THROWS(MaskableSum<data_t>{std::move(empty)});
    }

    GIVEN("a mask of invalid size")
    {

        IndexVector_t numCoeff{1};
        numCoeff[0] = 4;
        VolumeDescriptor dd(numCoeff);

        ConstantFunctional<data_t> constant(dd, 3.141);

        std::vector<bool> empty{};

        std::vector<std::unique_ptr<Functional<data_t>>> tmp1, tmp2, tmp3;
        tmp1.push_back(constant.clone());
        tmp2.push_back(constant.clone());
        tmp3.push_back(constant.clone());

        CHECK_THROWS(MaskableSum<data_t>{std::move(tmp1), empty});

        std::vector<bool> toomany(123);

        ;
        CHECK_THROWS(MaskableSum<data_t>{std::move(tmp2), toomany});

        MaskableSum<data_t> valid{std::move(tmp3)};
        CHECK_THROWS(valid.setMask(toomany));
    }

    GIVEN("two 1D Functionals with different domains")
    {
        IndexVector_t nc1{1}, nc2{1};
        nc1[0] = 4;
        nc2[0] = 5000;
        VolumeDescriptor d1{nc1}, d2{nc2};

        ConstantFunctional<data_t> c1{d1, 3.141}, c2{d2, 3.141};

        std::vector<std::unique_ptr<Functional<data_t>>> tmp;
        tmp.push_back(c1.clone());
        tmp.push_back(c2.clone());

        CHECK_THROWS(MaskableSum<data_t>{std::move(tmp)});
    }
}

TEST_CASE_TEMPLATE("MaskableSum: Clone and Compare", data_t, float, complex<float>, double,
                   complex<double>)
{
    GIVEN("two 1D Functionals with the same underlying functionals")
    {
        IndexVector_t numCoeff{1};
        numCoeff[0] = 4;
        VolumeDescriptor dd(numCoeff);

        ConstantFunctional<data_t> constant(dd, 3.141);

        std::vector<std::unique_ptr<Functional<data_t>>> tmp1, tmp2;
        tmp1.push_back(constant.clone());
        tmp2.push_back(constant.clone());

        MaskableSum<data_t> masked1{std::move(tmp1)};
        MaskableSum<data_t> masked2{std::move(tmp2)};

        WHEN("Wrapping equivalent functionals, equality should be given")
        {
            CHECK_EQ(masked1, masked2);
        }

        WHEN("Cloning one, they should compare equal")
        {
            auto masked3 = masked1.clone();
            CHECK_EQ(masked1, *masked3);
        }
    }
}

TEST_CASE_TEMPLATE("ConstantFunctional: Testing evaluation", data_t, float, complex<float>, double,
                   complex<double>)
{
    GIVEN("two constant Functionals")
    {
        IndexVector_t numCoeff{1};
        numCoeff[0] = 4;
        VolumeDescriptor dd{numCoeff};

        DataContainer<data_t> x = full<data_t>(dd, 456);

        std::vector<std::unique_ptr<Functional<data_t>>> tmp;
        tmp.push_back(std::make_unique<ConstantFunctional<data_t>>(dd, 5));
        tmp.push_back(std::make_unique<ConstantFunctional<data_t>>(dd, 123));

        MaskableSum<data_t> masked1(std::move(tmp));

        CHECK_EQ(masked1.evaluate(x), data_t{5 + 123});
    }
}

TEST_CASE_TEMPLATE("ConstantFunctional: Testing gradient", data_t, float, double)
{
    GIVEN("two constant Functionals")
    {
        IndexVector_t numCoeff{1};
        numCoeff[0] = 1;
        VolumeDescriptor dd{numCoeff};

        DataContainer<data_t> x = full<data_t>(dd, 456);

        std::vector<std::unique_ptr<Functional<data_t>>> tmp;
        tmp.push_back(std::make_unique<L2Squared<data_t>>(dd));
        tmp.push_back(std::make_unique<L2Squared<data_t>>(dd));

        MaskableSum<data_t> masked1(std::move(tmp));

        masked1.setAll();
        auto grad1 = masked1.getGradient(x);
        REQUIRE_UNARY(isApprox(grad1, 2 * x));

        masked1.setMask({true, false});
        auto grad2 = masked1.getGradient(x);
        REQUIRE_UNARY(isApprox(grad2, x));
    }
}
