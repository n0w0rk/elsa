#include "LB.h"
#include "DataContainer.h"
#include "LinearOperator.h"
#include "TypeCasts.hpp"
#include "Logger.h"

#include "spdlog/stopwatch.h"
#include "PowerIterations.h"

namespace elsa
{
    template <typename data_t>
    LB<data_t>::LB(const LinearOperator<data_t>& A, const DataContainer<data_t>& b,
                   const ProximalOperator<data_t>& prox, data_t mu, std::optional<data_t> beta,
                   data_t epsilon)
        : A_(A.clone()), b_(b), prox_(prox), mu_(mu), epsilon_(epsilon)
    {
        if (!beta.has_value()) {

            beta_ = data_t{2} / (mu_ * powerIterations(adjoint(*A_) * (*A_)));
            Logger::get("LB")->info("Step length is chosen to be: {:8.5}", beta_);

        } else {
            beta_ = *beta;
        }
    }

    template <typename data_t>
    auto LB<data_t>::solve(index_t iterations, std::optional<DataContainer<data_t>> x0)
        -> DataContainer<data_t>
    {
        spdlog::stopwatch iter_time;

        auto v = DataContainer<data_t>(A_->getDomainDescriptor());
        auto x = DataContainer<data_t>(A_->getDomainDescriptor());

        v = 0;

        if (x0.has_value()) {
            x = *x0;
        } else {
            x = 0;
        }

        auto residual = DataContainer<data_t>(b_.getDataDescriptor());

        for (int i = 0; i < iterations; ++i) {

            // x^{k+1} = mu * prox(v^k, 1)
            x = mu_ * prox_.apply(v, 1);

            // residual = b - Ax^{k+1}
            lincomb(1, b_, -1, A_->apply(x), residual);

            // v^{k+1} += beta * A^{*}(b - Ax^{k+1})
            v += beta_ * A_->applyAdjoint(residual);

            auto error = residual.squaredL2Norm() / b_.squaredL2Norm();
            if (residual.squaredL2Norm() / b_.squaredL2Norm() <= epsilon_) {
                Logger::get("LB")->info("SUCCESS: Reached convergence at {}/{} iteration", i + 1,
                                        iterations);
                return x;
            }

            Logger::get("LB")->info(
                "|iter: {:>6} | x: {:>12} | v: {:>12} | error: {:>12} | time: {:>8.3} |", i,
                x.squaredL2Norm(), v.squaredL2Norm(), error, iter_time);
        }

        Logger::get("LB")->warn("Failed to reach convergence at {} iterations", iterations);

        return x;
    }

    template <typename data_t>
    auto LB<data_t>::cloneImpl() const -> LB<data_t>*
    {
        return new LB<data_t>(*A_, b_, prox_, mu_, beta_, epsilon_);
    }

    template <typename data_t>
    auto LB<data_t>::isEqual(const Solver<data_t>& other) const -> bool
    {
        auto otherLb = downcast_safe<LB>(&other);
        if (!otherLb)
            return false;

        if (*A_ != *otherLb->A_)
            return false;

        if (b_ != otherLb->b_)
            return false;

        Logger::get("LB")->info("beta: {}, {}", beta_, otherLb->beta_);
        if (std::abs(beta_ - otherLb->beta_) > 1e-5)
            return false;

        Logger::get("LB")->info("mu: {}, {}", mu_, otherLb->mu_);
        if (mu_ != otherLb->mu_)
            return false;

        Logger::get("LB")->info("epsilon: {}, {}", epsilon_, otherLb->epsilon_);
        if (epsilon_ != otherLb->epsilon_)
            return false;

        return true;
    }

    // ------------------------------------------
    // explicit template instantiation
    template class LB<float>;
    template class LB<double>;
} // namespace elsa
