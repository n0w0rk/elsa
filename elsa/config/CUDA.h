#pragma once

namespace elsa::cuda
{
    void setDevice(int device);

    int getDeviceCount();
} // namespace elsa::cuda
