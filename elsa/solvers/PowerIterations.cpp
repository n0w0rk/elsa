#include "PowerIterations.h"
#include "DataContainer.h"
#include "Error.h"
#include "LinearOperator.h"
#include <cmath>

#include <iostream>

namespace elsa
{
    template <class data_t>
    data_t powerIterations(const LinearOperator<data_t>& op, index_t maxiters, real_t rtol,
                           real_t atol)
    {
        if (op.getDomainDescriptor().getNumberOfCoefficients()
            != op.getRangeDescriptor().getNumberOfCoefficients()) {
            throw LogicError("powerIterations: Operator for power iterations must be symmetric");
        }

        DataContainer<data_t> u(
            op.getDomainDescriptor(),
            Vector_t<data_t>::Random(op.getDomainDescriptor().getNumberOfCoefficients()));

        data_t norm = u.l2Norm();
        data_t normOld = norm;

        u /= norm;

        for (index_t i = 0; i < maxiters; i++) {
            op.apply(u, u);

            norm = u.l2Norm();
            if (norm == 0.0) {
                throw Error("powerIterations: Reached 0 vector after {} iterations", i);
            }

            if (std::isinf(norm)) {
                throw Error("powerIterations: Reached non-finite vector after {} iterations", i);
            }

            if (std::abs(norm - normOld) < atol + rtol * std::abs(norm)) {
                break;
            } else {
                u /= norm;
                normOld = norm;
            }
        }

        return norm;
    }

    template float powerIterations(const LinearOperator<float>&, index_t, real_t, real_t);
    template double powerIterations(const LinearOperator<double>&, index_t, real_t, real_t);
} // namespace elsa
