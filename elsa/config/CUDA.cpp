#include "CUDA.h"

#include <stdexcept>

#include "cuda_runtime_api.h"

namespace elsa::cuda
{
    void setDevice(int device)
    {
        if (device > getDeviceCount()) {
            throw std::runtime_error("Invalid CUDA device.");
        }

        if (cudaSetDevice(device) != cudaSuccess) {
            throw std::runtime_error("Failed to set CUDA device.");
        }
    }

    int getDeviceCount()
    {
        int count;
        if (cudaGetDeviceCount(&count) != cudaSuccess) {
            throw std::runtime_error("Failed to get CUDA device count.");
        }
        return count;
    }
} // namespace elsa::cuda
