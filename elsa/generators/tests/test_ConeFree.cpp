/**
 * @file test_ConeFree.cpp
 *
 * @brief Test for ConeFree
 */

#include "doctest/doctest.h"

#include "PhantomDefines.h"
#include "ConeFree.h"

using namespace elsa;
using namespace doctest;

TEST_CASE("ConeFree tests")
{
    GIVEN("A rotated ConeFree ")
    {
        index_t size = 10;
        IndexVector_t sizes(3);
        sizes << size, size, size;

        phantoms::Vec3i center;
        center << 5.0, 5.0, 5.0;

        double length{4.};
        double amplit{1.};
        double radius1{2.};
        double radius2{4.};

        phantoms::Vec3X<double> eulers;
        eulers << 0.0, 45.0, 45.0;

        VolumeDescriptor dd(sizes);
        DataContainer<double> dc(dd);
        dc = 0;

        WHEN("Rasterize")
        {

            phantoms::ConeFree<double> ec{amplit, center, radius1, radius2, length, eulers};

            phantoms::rasterize<phantoms::Blending::ADDITION>(ec, dd, dc);

            IndexVector_t idx(3);

            WHEN("Check empty volume")
            {
                idx << 2, 5, 5;
                REQUIRE_EQ(dc(idx), 0);

                idx << 7, 3, 3;
                REQUIRE_EQ(dc(idx), 0);

                idx << 3, 7, 7;
            }
            WHEN("Check filled volume")
            {

                idx << 5, 5, 5;
                REQUIRE_EQ(dc(idx), amplit);

                idx << 6, 4, 4;
                REQUIRE_EQ(dc(idx), amplit);

                idx << 4, 6, 6;
                REQUIRE_EQ(dc(idx), amplit);
            }
        }
    }
}
