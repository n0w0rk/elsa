#pragma once
#include "PhantomDefines.h"

namespace elsa::phantoms
{

    template <typename data_t = double>
    class Cone
    {

    private:
        Orientation _orientation;
        data_t _amplit;
        Vec3i _center;

        data_t _radius1;
        data_t _radius2;
        data_t _length;

        data_t _start;

        data_t rSlope;

    public:
        /**
         * Creates a Cone stump along any major axis. Axis is defined by the orientation.
         * End radii are defined by radius1 and radius2. The length is the distance from one end to
         * the other.
         *
         * @param o Orientation along X_AXIS, Y_AXIS or Z_AXIS
         * @param amlpit amplitude wich is added to the voxel on rasterization
         * @param center center of the object
         * @param radius1 radius of one cone end
         * @param radius2 radius of the other cone end
         * @param length the length from one side throught the center to the other side
         *
         */
        Cone(Orientation o, data_t amplit, Vec3i center, data_t radius1, data_t radius2,
             data_t length);

        /**
         * @brief returns the orientation to color the voxel
         */
        Orientation getOrientation() const { return _orientation; };
        /**
         * @brief returns the amplitude to color the voxel
         */
        const data_t getAmplitude() const { return _amplit; };
        /**
         * @brief returns the center of the Cone
         */
        const Vec3i& getCenter() const { return _center; };
        /**
         * @brief returns the radius of one end of the Cone
         */
        const data_t getRadius1() const { return _radius1; };
        /**
         * @brief returns the radius of one end of the Cone
         */
        const data_t getRadius2() const { return _radius2; };
        /**
         * @brief returns the length of the Cone
         */
        const data_t getLength() const { return _length; };
        /**
         * @brief returns the start of the Cone
         */
        const data_t getStart() const { return _start; };

        bool isInCone(const Vec3i& idx) const;
    };

    /**
     * @brief Rasterizes the given Cone in the given data container.
     */
    template <Blending b, typename data_t>
    void rasterize(Cone<data_t>& cone, VolumeDescriptor& dd, DataContainer<data_t>& dc);

} // namespace elsa::phantoms

template <typename data_t>
struct fmt::formatter<elsa::phantoms::Cone<data_t>> : fmt::formatter<std::string> {
    auto format(const elsa::phantoms::Cone<data_t>& cone, format_context& ctx)
        -> decltype(ctx.out())
    {
        auto _center = cone.getCenter();
        auto _amplit = cone.getAmplitude();
        auto _radius1 = cone.getRadius1();
        auto _radius2 = cone.getRadius2();
        auto _length = cone.getLength();
        auto _orientation = cone.getOrientation();

        return format_to(ctx.out(),
                         "Cone with amplitude {}, Center ({},{},{}), radii r1 ({}),"
                         "r2 ({}), length {} along orientation {} ",
                         _amplit, _center[elsa::phantoms::INDEX_X],
                         _center[elsa::phantoms::INDEX_Y], _center[elsa::phantoms::INDEX_Z],
                         _radius1, _radius2, _length, getString(_orientation));
    }
};
