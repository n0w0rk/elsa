import argparse
from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial import Delaunay
import pyelsa as elsa


def read_weighted_recon_dirs():
    recon_dirs_weights_comb = np.loadtxt("./crossed_sticks2/vecs.csv", comments="#", delimiter=" ")
    assert (recon_dirs_weights_comb.shape[1] == 4)
    recon_dirs = recon_dirs_weights_comb[:, :3].copy()
    recon_weights = recon_dirs_weights_comb[:, -1:].reshape(-1).copy()
    return recon_dirs, recon_weights


def create_delauney_edges(points):
    tri = Delaunay(points)
    # using a set to remove duplicates
    edges = set()
    for row in tri.simplices:
        for i in range(3):
            edge = (row[i], row[(i + 1) % 3])
            edges.add(tuple(sorted(edge)))
    edges = np.array(list(edges))
    return edges


def get_direction_color_2d(v):
    if not len(v) == 2:
        raise ValueError("Input vector must have two components.")

    # color samples (last component is a scale factor, to reduce/increase
    # the impact of a sample with respect to other samples)
    samples = np.array([
        [0.8, 0, 1, 0, 0, 2],
        [0, 0.8, 0, 0, 1, 2],
        [0.3, 0.3, 0, 1, 0, 1.8],
    ])

    vn = v / (np.linalg.norm(v) + np.finfo(float).eps)
    n = samples.shape[0]

    # run through the samples, accumulate, and normalize afterward
    rgb = np.zeros(3)
    for i in range(n):
        sv = samples[i, 0:2]
        sv = sv / np.linalg.norm(sv)
        sc = samples[i, 2:5]
        sc = sc / max(sc)
        sf = samples[i, 5]

        # compute angle between sample and query vector
        factor = np.arcsin(np.abs(np.dot(vn, sv))) / (np.pi / 2) * sf
        rgb = rgb + factor * sc

    rgb = rgb / (max(rgb) + np.finfo(float).eps)
    return rgb


def visualise_fiber_orientations(dirs, mags, ax, step, threshold, max_val):
    sy, sx, _ = dirs.shape
    subplots = []

    for y in range(0, sy, step):
        for x in range(0, sx, step):
            radius = mags[y, x]

            if radius < threshold:
                continue
            color = get_direction_color_2d(dirs[y, x, :])
            saturation = 1
            color *= saturation

            dx, dy = dirs[y, x, :] * (1.2 - ((2 * radius) / max_val))

            line = ax.plot([x - dx, x + dx],
                           [y - dy, y + dy],
                           '-', color=tuple(color))
            subplots.append(line)

    return subplots


def map_recon_model_to_title(path):
    path = str(path)
    if "glogd" in path:
        return "Gaussian log(d)"
    elif "gd" in path:
        return "Gaussian d"
    elif "gb" in path:
        return "Rician b (approximated by Gaussian)"
    else:
        return "Rician b"


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='')
    parser.add_argument("--dci", type=Path, required=True,
                        help="Path to recon file containing scattering information")
    parser.add_argument("--amp", type=Path, required=True,
                        help="Path to recon file containing attenuation information")
    parser.add_argument("--step", type=int, default=2, help="Plots every n-th direction")

    args = parser.parse_args()
    dci_path = args.dci
    amp_path = args.amp
    step = args.step

    recon_dci = np.load(dci_path)
    recon_amp = np.load(amp_path)
    # load reconstructions of scattering and attenuation information

    num_slice, sx, sy, sz = recon_dci.shape
    vol_sz = [sx, sy, sz]
    single_vol_desc = elsa.VolumeDescriptor(vol_sz)
    vol_desc = elsa.IdenticalBlocksDescriptor(num_slice, single_vol_desc)
    recon_dci_dc = elsa.DataContainer(recon_dci, vol_desc)
    # read the reconstruction and create the sphField data container

    recon_dirs, recon_weights = read_weighted_recon_dirs()
    # get the reconstruction pattern

    delauneyEdges = create_delauney_edges(recon_dirs)
    # get the delauney edges

    maxNumMaximaExtracted = 4
    sph_degree = 4
    sfInfo = elsa.SphericalFunctionInformation(recon_dirs, recon_weights,
                                               elsa.AXDTOperator.Symmetry.Even, sph_degree)
    # create the spherical function information used for the extraction
    r, v, rEdge, vEdge, mean, gfa = elsa.extractFiberDirection(recon_dci_dc, sfInfo,
                                                               delauneyEdges, maxNumMaximaExtracted)
    r_numpy = np.asarray(r)
    v_numpy = np.asarray(v)
    # convert the results to numpy arrays

    r_numpy = r_numpy[0, :, :, :]
    # take the highest maxima volume magnitudes
    v_numpy = v_numpy[0, :, :, :, :]
    # take the highest maxima volume directions
    v_numpy = np.transpose(v_numpy, (1, 2, 3, 0))

    mid = sx // 2
    fig, ax = plt.subplots(1, 1, figsize=(25, 15))
    fig.suptitle('Fiber orientations - ' + map_recon_model_to_title(dci_path), fontsize=28)

    min_val, max_val = np.min(recon_amp[mid, :, :]), np.max(recon_amp[mid, :, :])
    ax.imshow(recon_amp[mid, :, :], cmap='gray', vmin=0.2 * min_val, vmax=0.2 * max_val)
    visualise_fiber_orientations(np.stack((v_numpy[mid, :, :, 1], v_numpy[mid, :, :, 2]), axis=2),
                                 r_numpy[mid, :, :],
                                 ax, step,
                                 np.mean(r_numpy[mid, :, :]) + 1 * np.std(r_numpy[mid, :, :]), np.max(r_numpy))
    plt.show()

    fig2, ax2 = plt.subplots(1, 1, figsize=(25, 15))
    fig2.suptitle('Fiber orientations - ' + map_recon_model_to_title(dci_path), fontsize=28)
    fig2.tight_layout(pad=3.0)

    ax2.imshow(recon_amp[mid, :, :], cmap='gray', vmin=0.2 * min_val, vmax=0.2 * max_val)
    visualise_fiber_orientations(np.stack((v_numpy[mid, :, :, 2], v_numpy[mid, :, :, 1]), axis=2),
                                 r_numpy[mid, :, :],
                                 ax2, step,
                                 np.mean(r_numpy[mid, :, :]) + 1 * np.std(r_numpy[mid, :, :]), np.max(r_numpy))
    plt.show()