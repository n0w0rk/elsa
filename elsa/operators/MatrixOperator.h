#pragma once

#include "ContiguousStorage.h"
#include "DataDescriptor.h"
#include "LinearOperator.h"
#include "elsaDefines.h"
#include <Eigen/src/Core/Map.h>
#include <Eigen/src/Core/Matrix.h>
#include <Eigen/src/Core/util/Constants.h>

namespace elsa
{
    /**
     */
    template <typename data_t = real_t>
    class MatrixOperator : public LinearOperator<data_t>
    {
    public:
        /**
         * @brief Constructor for the identity operator, specifying the domain (= range).
         *
         * @param[in] descriptor DataDescriptor describing the domain and range of the operator
         */
        explicit MatrixOperator(const Matrix_t<data_t>& mat);

        MatrixOperator(const DataDescriptor& domain, const DataDescriptor& range,
                       const Matrix_t<data_t>& mat);

        /// default destructor
        ~MatrixOperator() override = default;

    protected:
        /// default copy constructor, hidden from non-derived classes to prevent potential slicing
        MatrixOperator(const MatrixOperator<data_t>&) = default;

        /**
         * @brief apply the identity operator A to x, i.e. Ax = x
         *
         * @param[in] x input DataContainer (in the domain of the operator)
         * @param[out] Ax output DataContainer (in the range of the operator)
         */
        void applyImpl(const DataContainer<data_t>& x, DataContainer<data_t>& Ax) const override;

        /**
         * @brief apply the adjoint of the identity operator A to y, i.e. A^ty = y
         *
         * @param[in] y input DataContainer (in the range of the operator)
         * @param[out] Aty output DataContainer (in the domain of the operator)
         */
        void applyAdjointImpl(const DataContainer<data_t>& y,
                              DataContainer<data_t>& Aty) const override;

        /// implement the polymorphic clone operation
        MatrixOperator<data_t>* cloneImpl() const override;

        /// implement the polymorphic comparison operation
        bool isEqual(const LinearOperator<data_t>& other) const override;

    private:
        ContiguousStorage<data_t> storage_;

        Eigen::Map<Matrix_t<data_t>> mat_;
    };
} // namespace elsa
