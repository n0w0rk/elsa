import numpy as np
import pyelsa as elsa
import tifffile
from htc import reconstructionTV

def solve_with_TGV(A, b, niters, sigma=10, lambd=1, alpha0=1, alpha1=2, tau=None):
    w_desc = elsa.IdenticalBlocksDescriptor(2, A.getDomainDescriptor())

    #first row of K
    E1 = elsa.ZeroOperator(w_desc, A.getRangeDescriptor())
    row1 = elsa.BlockLinearOperator(
        [A, E1],
        elsa.BlockLinearOperator.BlockType.COL,
    )

    #second row of K
    FDiff = elsa.FiniteDifferences(A.getDomainDescriptor())
    Scale = elsa.Scaling(w_desc, -1)
    row2 = elsa.BlockLinearOperator(
        [FDiff, Scale],
        elsa.BlockLinearOperator.BlockType.COL,
    )

    #third row of K
    SymDer = elsa.SymmetrizedDerivative(w_desc)
    E3 = elsa.ZeroOperator(A.getDomainDescriptor(), SymDer.getRangeDescriptor())
    row3 = elsa.BlockLinearOperator(
        [E3, SymDer],
        elsa.BlockLinearOperator.BlockType.COL,
    )

    K = elsa.BlockLinearOperator(
        [row1, row2, row3],
        elsa.BlockLinearOperator.BlockType.ROW,
    )

    #proximals for each row
    proxg1 = elsa.ProximalL2Squared(b, lambd)
    proxg2 = elsa.ProximalL1(alpha0)
    proxg3 = elsa.ProximalL1(alpha1)
    proxg = elsa.CombinedProximal(proxg1, proxg2, proxg3)

    #non-negativity constrain
    proxf = elsa.ProximalBoxConstraint(0)

    admm = elsa.LinearizedADMM(K, proxf, proxg, sigma, tau)

    #need to materialize the first block, because the result is [x, w]
    return elsa.materialize(admm.solve(niters).getBlock(0))

def example_shepp(s: int):
    size = np.array([s] * 2)
    phantom = elsa.phantoms.modifiedSheppLogan(size)
    volDesc = phantom.getDataDescriptor()

    distance = size[0]
    sinoDesc = elsa.CircleTrajectoryGenerator.trajectoryFromAngles(
        np.arange(0, 180, 1), phantom.getDataDescriptor(
        ), distance * 100.0, distance
    )

    projector = elsa.JosephsMethod(phantom.getDataDescriptor(), sinoDesc)
    sinogram = projector.apply(phantom)

    recon = solve_with_TGV(projector, sinogram, 50)
    tifffile.imwrite("./TGV_shepp.tif", np.array(recon))


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Process some integers.")
    parser.add_argument("--size", default=128, type=int,
                        help="size of reconstruction")

    args = parser.parse_args()
    example_shepp(args.size)
