import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, TextBox, Button, CheckButtons

# geometry visualization

RED = "red"
BLUE = "blue"
GREEN = "green"
MAGENTA = "magenta"
BROWN = "brown"
BLACK = "black"
DASHED = "--"
SOLID = "-"
ARROW_LENGTH_RATIO = 0.1
ARROW_SCALE = 1
MARKER_SIZE = 100


def plot_lines(ax, points, color, linestyle):
    points = np.array(points)

    # use slicing to create start and end points
    start_points = points[:-1]
    end_points = points[1:]

    # plot lines
    for start, end in zip(start_points, end_points):
        ax.plot(*zip(start, end), color=color, linestyle=linestyle)


def plot_coordinate_system(ax, center, directions, length, color, labels):
    number_of_dimensions = len(center)

    for direction, label in zip(directions, labels):
        # calculate end point of arrow
        scaled_direction = length * direction
        end_point = center + scaled_direction

        # plot arrow and label
        if number_of_dimensions == 3:
            ax.quiver(
                *center,
                *scaled_direction,
                color=color,
                arrow_length_ratio=ARROW_LENGTH_RATIO,
            )
        elif number_of_dimensions == 2:
            ax.quiver(
                *center,
                *scaled_direction,
                color=color,
                angles="xy",
                scale_units="xy",
                scale=ARROW_SCALE,
            )
        ax.text(*end_point, label, color=color)


def point_along_vector(start, vector, distance):
    normalized_vector = vector / np.linalg.norm(vector)
    start_to_end_vector = distance * normalized_vector
    return start + start_to_end_vector


def plot_trajectory(ax, sino_descriptor):
    # extract camera centers (source positions) for each geometry pose
    camera_centers = [
        sino_descriptor.getGeometryAt(i).getCameraCenter()
        for i in range(sino_descriptor.getNumberOfGeometryPoses())
    ]

    # unpack list of source positions into separate coordinate lists
    coordinates = list(zip(*camera_centers))

    ax.plot(*coordinates, color=MAGENTA, linestyle=SOLID, label="trajectory")


def plot_source(ax, source_pos):
    ax.scatter(*source_pos, c=RED, label="source position")


def plot_detector(
    ax, detector_center, detector_width, detector_height, rotation_matrix, source_pos
):
    number_of_dimensions = len(detector_center)

    detector_width_half = detector_width / 2
    detector_height_half = detector_height / 2
    x_axis_right = rotation_matrix[0]
    y_axis_up = rotation_matrix[1]

    if number_of_dimensions == 3:
        detector_corners = [
            detector_center
            + detector_width_half * x_axis_right
            - detector_height_half * y_axis_up,
            detector_center
            - detector_width_half * x_axis_right
            - detector_height_half * y_axis_up,
            detector_center
            - detector_width_half * x_axis_right
            + detector_height_half * y_axis_up,
            detector_center
            + detector_width_half * x_axis_right
            + detector_height_half * y_axis_up,
        ]

        # close loop by appending first corner at end of list
        detector_corners = detector_corners + [detector_corners[0]]
    else:
        detector_corners = [
            detector_center + detector_width_half * x_axis_right,
            detector_center - detector_width_half * x_axis_right,
        ]

    # plot detector center as blue dot
    ax.scatter(
        *detector_center,
        c=BLUE,
        label="detector center",
    )

    # connect corners of detector with blue dashed lines
    plot_lines(ax, detector_corners, BLUE, DASHED)

    # connect corners of detector with source position with red dashed lines
    for corner in detector_corners:
        plot_lines(ax, [source_pos, corner], RED, DASHED)

    # connect detector center with source position with red solid line
    plot_lines(ax, [source_pos, detector_center], RED, SOLID)

    # mark pixel 0 on detector with blue x
    ax.scatter(
        *(detector_corners[1]),
        c=BLUE,
        marker="x",
        s=MARKER_SIZE,
        label="data origin (pixel 0)",
    )

    # return pixel 0
    return detector_corners[1]


def plot_volume(ax, volume_descriptor):
    number_of_dimensions = volume_descriptor.getNumberOfDimensions()

    volume_center = volume_descriptor.getLocationOfOrigin()
    volume_dimensions = volume_descriptor.getNumberOfCoefficientsPerDimension()
    volume_dimensions_half = [dim / 2 for dim in volume_dimensions]
    volume_corners = []

    # number of corners is 2^dimensionality (4 for 2D, 8 for 3D)
    for i in range(2**number_of_dimensions):
        corner = []
        for j in range(number_of_dimensions):
            # add or subtract half dimension based on binary representation of i
            corner.append(
                volume_center[j] + (-1) ** ((i >> j) & 1) * volume_dimensions_half[j]
            )
        volume_corners.append(tuple(corner))

    # plot volume center as green dot
    ax.scatter(
        *volume_center,
        c=GREEN,
        label="volume center",
    )

    edges_2d = [(0, 1), (0, 2), (1, 3), (2, 3)]
    edges_3d = [
        (0, 1),
        (0, 2),
        (0, 4),
        (1, 3),
        (1, 5),
        (2, 3),
        (2, 6),
        (3, 7),
        (4, 5),
        (4, 6),
        (5, 7),
        (6, 7),
    ]

    edges = edges_2d if number_of_dimensions == 2 else edges_3d

    # connect corners of volume with green dashed lines
    for edge in edges:
        plot_lines(
            ax, [volume_corners[edge[0]], volume_corners[edge[1]]], GREEN, DASHED
        )

    # mark voxel 0 on volume with green x
    ax.scatter(
        *(volume_corners[-1]),
        c=GREEN,
        marker="x",
        s=MARKER_SIZE,
        label="data origin (voxel 0)",
    )

    # return voxel 0
    return volume_corners[-1]


def plot_geometry(ax, sino_descriptor, volume_descriptor, current_pose):
    number_of_dimensions = sino_descriptor.getNumberOfDimensions()

    # extract geometry information
    geometry = sino_descriptor.getGeometryAt(current_pose)
    source_pos = geometry.getCameraCenter()
    rotation_matrix = geometry.getRotationMatrix()

    # world origin
    ax.scatter(*([0] * number_of_dimensions), c=BLACK, label="world origin")

    empty_coords = [[] for _ in range(number_of_dimensions)]

    plot_source(ax, source_pos)

    # define camera coordinate system directions and labels
    camera_coord_sys_directions = rotation_matrix[:number_of_dimensions]
    camera_coord_sys_labels = ["X", "Y", "Z"]

    # plot camera coordinate system
    plot_coordinate_system(
        ax,
        source_pos,
        camera_coord_sys_directions,
        max(volume_descriptor.getNumberOfCoefficientsPerDimension()) / 2,
        BLACK,
        camera_coord_sys_labels[:number_of_dimensions],
    )

    # add camera coordinate system to legend
    ax.plot(
        *empty_coords, color=BLACK, linestyle=SOLID, label="camera coordinate system"
    )

    # calculate center of rotation
    center_of_rotation = point_along_vector(
        source_pos, rotation_matrix[-1], geometry.getSourceDetectorDistance() / 2
    )

    ax.scatter(
        *center_of_rotation,
        c=BROWN,
        label="center of rotation",
    )

    # calculate detector center
    detector_center = point_along_vector(
        source_pos, rotation_matrix[-1], geometry.getSourceDetectorDistance()
    )

    detector_width = (
        sino_descriptor.getNumberOfCoefficientsPerDimension()[0]
        * sino_descriptor.getSpacingPerDimension()[0]
    )
    detector_height = (
        sino_descriptor.getNumberOfCoefficientsPerDimension()[1]
        * sino_descriptor.getSpacingPerDimension()[1]
    )

    pixel_0 = plot_detector(
        ax,
        detector_center,
        detector_width,
        detector_height,
        rotation_matrix,
        source_pos,
    )

    # add detector to legend
    ax.plot(*empty_coords, color=BLUE, linestyle=DASHED, label="detector")

    # define detector coordinate system directions and labels
    detector_coord_sys_directions = rotation_matrix[: (number_of_dimensions - 1)]
    detector_coord_sys_labels = ["Dx", "Dy"]

    # plot detector coordinate system
    plot_coordinate_system(
        ax,
        detector_center,
        detector_coord_sys_directions,
        min(detector_width, detector_height) / 5,
        BLUE,
        detector_coord_sys_labels[: (number_of_dimensions - 1)],
    )

    voxel_0 = plot_volume(ax, volume_descriptor)
    # add volume to legend
    ax.plot(*empty_coords, color=GREEN, linestyle=DASHED, label="image geometry")

    plot_trajectory(ax, sino_descriptor)

    # set labels and aspect ratio
    ax.set_xlabel("X axis")
    ax.set_ylabel("Y axis")

    if number_of_dimensions == 3:
        ax.set_zlabel("Z axis")
        ax.set_box_aspect([1, 1, 1])  # equal aspect ratio
        ax.view_init(elev=ax.elev, azim=ax.azim)  # keep viewing angle

    ax.legend(loc="upper center", bbox_to_anchor=(0.5, -0.1), ncol=2)

    return (
        source_pos,
        detector_center,
        center_of_rotation,
        pixel_0,
        voxel_0,
    )


def coords_as_text(coords, number_of_dimensions):
    # format coordinates as text
    if number_of_dimensions == 3:
        coords_text = f"({coords[0]:.2f}, {coords[1]:.2f}, {coords[2]:.2f})"
    else:
        coords_text = f"({coords[0]:.2f}, {coords[1]:.2f})"

    return coords_text


def create_boxes(
    fig,
    source_position,
    detector_center,
    volume_center,
    center_of_rotation,
    pixel_0,
    voxel_0,
    number_of_dimensions,
):
    # initialize text boxes for coordinates
    source_position_axis = fig.add_axes([0.6, 0.2, 0.2, 0.05])
    source_position_box = plt.text(
        0.5,
        0.5,
        f"Source: {coords_as_text(source_position, number_of_dimensions)}",
        ha="left",
        va="center",
        transform=source_position_axis.transAxes,
    )
    source_position_axis.axis("off")

    detector_center_axis = fig.add_axes([0.6, 0.175, 0.2, 0.05])
    detector_center_box = plt.text(
        0.5,
        0.5,
        f"Detector: {coords_as_text(detector_center, number_of_dimensions)}",
        ha="left",
        va="center",
        transform=detector_center_axis.transAxes,
    )
    detector_center_axis.axis("off")

    volume_center_axis = fig.add_axes([0.6, 0.15, 0.2, 0.05])
    volume_center_box = plt.text(
        0.5,
        0.5,
        f"Volume: {coords_as_text(volume_center, number_of_dimensions)}",
        ha="left",
        va="center",
        transform=volume_center_axis.transAxes,
    )
    volume_center_axis.axis("off")

    center_of_rotation_axis = fig.add_axes([0.6, 0.125, 0.2, 0.05])
    center_of_rotation_box = plt.text(
        0.5,
        0.5,
        f"Center of rotation: {coords_as_text(center_of_rotation, number_of_dimensions)}",
        ha="left",
        va="center",
        transform=center_of_rotation_axis.transAxes,
    )
    center_of_rotation_axis.axis("off")

    pixel_0_axis = fig.add_axes([0.6, 0.1, 0.2, 0.05])
    pixel_0_box = plt.text(
        0.5,
        0.5,
        f"Pixel 0: {coords_as_text(pixel_0, number_of_dimensions)}",
        ha="left",
        va="center",
        transform=pixel_0_axis.transAxes,
    )
    pixel_0_axis.axis("off")

    voxel_0_axis = fig.add_axes([0.6, 0.075, 0.2, 0.05])
    voxel_0_box = plt.text(
        0.5,
        0.5,
        f"Voxel 0: {coords_as_text(voxel_0, number_of_dimensions)}",
        ha="left",
        va="center",
        transform=voxel_0_axis.transAxes,
    )
    voxel_0_axis.axis("off")

    return (
        source_position_box,
        detector_center_box,
        volume_center_box,
        center_of_rotation_box,
        pixel_0_box,
        voxel_0_box,
    )


def visualize_geometry(sino_descriptor, volume_descriptor):
    """
    Visualizes the geometry of an X-ray CT setup, illustrating the positioning and
    orientation of the X-ray source, detector, and the image volume for each geometry pose.
    This function supports both 2D and 3D setups.

    Parameters
    ----------
    sino_descriptor : object
        A descriptor object containing sinogram data and geometry information.
    volume_descriptor : object
        A descriptor object providing details about the image volume.

    Features
    --------
    - Interactive slider to navigate through different geometry poses.
    - Displays key components such as source position, detector center, volume center,
      center of rotation, pixel 0, and voxel 0.
    - Provides a coordinate system representation for camera and detector.
    - Visualizes the trajectory of the camera.
    """

    number_of_dimensions = sino_descriptor.getNumberOfDimensions()

    fig = plt.figure(figsize=(12, 8))

    if number_of_dimensions == 3:
        ax = fig.add_axes([0.1, 0.3, 0.8, 0.6], projection="3d")
    elif number_of_dimensions == 2:
        ax = fig.add_axes([0.1, 0.3, 0.8, 0.6])
    else:
        raise ValueError("Number of dimensions must be 2 or 3.")

    plt.subplots_adjust(bottom=0.25)

    # plot first pose
    (
        source_pos,
        detector_center,
        center_of_rotation,
        pixel_0,
        voxel_0,
    ) = plot_geometry(ax, sino_descriptor, volume_descriptor, 0)

    # get axis limits
    xlim = ax.get_xlim()
    ylim = ax.get_ylim()

    if number_of_dimensions == 3:
        zlim = ax.get_zlim()

    # buffer to add to axis limits
    buffer = sino_descriptor.getGeometryAt(0).getSourceDetectorDistance() * 0.25

    ax.set_position([0.1, 0.3, 0.8, 0.6])

    # set axis limits
    ax.set_xlim([xlim[0] - buffer, xlim[1] + buffer])
    ax.set_ylim([ylim[0] - buffer, ylim[1] + buffer])

    if number_of_dimensions == 3:
        ax.set_zlim([zlim[0] - buffer, zlim[1] + buffer])

    (
        source_position_box,
        detector_center_box,
        volume_center_box,
        center_of_rotation_box,
        pixel_0_box,
        voxel_0_box,
    ) = create_boxes(
        fig,
        source_pos,
        detector_center,
        volume_descriptor.getLocationOfOrigin(),
        center_of_rotation,
        pixel_0,
        voxel_0,
        number_of_dimensions,
    )

    # create text box for slider
    ax_slider_box = plt.axes([0.2, 0.1, 0.1, 0.05])
    slider_box = TextBox(ax_slider_box, "Set Pose: ")
    slider_box.set_val(0)

    # define submission handler for slider box
    def submit_slider_box(text):
        try:
            pose = int(text)
            if 0 <= pose < sino_descriptor.getNumberOfGeometryPoses():
                slider.set_val(pose)  # set slider value and update plot
            else:
                print(
                    "Pose out of range. Please enter a number between 0 and",
                    sino_descriptor.getNumberOfGeometryPoses() - 1,
                )
        except ValueError:
            print("Invalid input. Please enter an integer.")

    slider_box.on_submit(submit_slider_box)

    # create slider
    ax_slider = plt.axes([0.2, 0.03, 0.65, 0.03])
    slider = Slider(
        ax_slider,
        "Geometry Pose",
        0,
        sino_descriptor.getNumberOfGeometryPoses() - 1,
        valinit=0,
        valfmt="%0.0f",
    )

    # update function for slider
    def slider_update(val):
        pose = int(slider.val)

        # clear axes and plot new geometry
        ax.clear()

        # set axis limits to old ones
        ax.set_xlim([xlim[0] - buffer, xlim[1] + buffer])
        ax.set_ylim([ylim[0] - buffer, ylim[1] + buffer])

        if number_of_dimensions == 3:
            ax.set_zlim([zlim[0] - buffer, zlim[1] + buffer])

        (
            source_pos,
            detector_center,
            center_of_rotation,
            pixel_0,
            voxel_0,
        ) = plot_geometry(ax, sino_descriptor, volume_descriptor, pose)

        ax.set_position([0.1, 0.3, 0.8, 0.6])

        # update boxes
        source_position_box.set_text(
            f"Source: {coords_as_text(source_pos, number_of_dimensions)}"
        )
        detector_center_box.set_text(
            f"Detector: {coords_as_text(detector_center, number_of_dimensions)}"
        )
        volume_center_box.set_text(
            f"Volume: {coords_as_text(volume_descriptor.getLocationOfOrigin(), number_of_dimensions)}"
        )
        center_of_rotation_box.set_text(
            f"Center of rotation: {coords_as_text(center_of_rotation, number_of_dimensions)}"
        )
        pixel_0_box.set_text(
            f"Pixel 0: {coords_as_text(pixel_0, number_of_dimensions)}"
        )
        voxel_0_box.set_text(
            f"Voxel 0: {coords_as_text(voxel_0, number_of_dimensions)}"
        )

        slider_box.set_val(pose)  # reset slider box value
        fig.canvas.draw_idle()  # redraw figure

    # call update function on slider value change
    slider.on_changed(slider_update)

    plt.draw()
    plt.show()


# reconstruction visualization


# fix colorbar layout, taken from https://joseph-long.com/writing/colorbars/
def colorbar(mappable):
    from mpl_toolkits.axes_grid1 import make_axes_locatable
    import matplotlib.pyplot as plt

    last_axes = plt.gca()
    ax = mappable.axes
    fig = ax.figure
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = fig.colorbar(mappable, cax=cax)
    plt.sca(last_axes)
    return cbar


class SliceTracker:
    """
    This class is responsible for tracking and updating the current slice of the dimension(s) being displayed.
    It also handles user interactions like scrolling to navigate through slices.
    """

    def __init__(
        self,
        ax,
        image_data,
        slice_dimension,
        cmap,
        center_slice_button_axis,
        dimension_name,
        trackers,  # list of other SliceTracker instances for synchronization
        sync_button,
        scroll_step_button,
    ):
        # initialization
        self.ax = ax
        self.image_data = image_data
        self.slice_dimension = slice_dimension
        self.slices = image_data.shape[slice_dimension]
        self.index = self.slices // 2  # start at center slice
        self.cmap = cmap
        self.image = ax.imshow(self.get_slice(self.index), cmap=self.cmap)

        self.colorbar = colorbar(self.image)
        self.ax.set_title(f"{dimension_name}")

        # create and setup 'Center Slice' button
        self.center_slice_button = Button(center_slice_button_axis, "Center Slice")
        self.center_slice_button.on_clicked(self.center_slice)

        # references to other trackers and control elements
        self.trackers = trackers
        self.sync_button = sync_button
        self.scroll_step_button = scroll_step_button

        # intialize plot
        self.update()

    def sync_slices(self, new_index):
        # check if sync_button is not None before accessing its status
        if self.sync_button and self.sync_button.status[0]:
            for tracker in self.trackers:
                tracker.index = new_index
                tracker.update()

    def get_slice(self, idx):
        if self.slice_dimension == 0:
            return self.image_data[idx, :, :]
        elif self.slice_dimension == 1:
            return self.image_data[:, idx, :]
        else:
            return self.image_data[:, :, idx]

    def center_slice(self, event):
        center_index = self.slices // 2
        self.index = center_index
        self.update()
        self.sync_slices(center_index)

    def on_scroll(self, event):
        if event.inaxes == self.ax:
            if event.button == "up":
                self.index = (self.index + self.scroll_step_button.value) % self.slices
            else:
                self.index = (self.index - self.scroll_step_button.value) % self.slices
            self.update()
            self.sync_slices(self.index)

    def update(self):
        self.image.set_data(self.get_slice(self.index))
        self.ax.set_ylabel(f"Slice {self.index} of {self.slices}")
        self.image.axes.figure.canvas.draw()


class ScrollStep:
    """
    This class represents the TextBox widget that allows users to set the number of slices to move per scroll.
    """

    def __init__(self, ax, step_increase_ax, step_decrease_ax):
        self.value = 1  # default step size

        # create TextBox widget
        self.box = TextBox(ax, "Scroll step size:", initial=str(self.value))
        self.box.on_submit(self.submit)

        # create buttons for increasing and decreasing step size
        self.button_up = Button(step_increase_ax, "+")
        self.button_down = Button(step_decrease_ax, "-")

        # connect buttons to respective callback functions
        self.button_up.on_clicked(self.increase_step)
        self.button_down.on_clicked(self.decrease_step)

    def submit(self, text):
        # update value when user submits number in TextBox
        try:
            self.value = int(text)
        except ValueError:
            self.box.set_val(str(self.value))  # reset box value if invalid input

    def increase_step(self, event):
        self.value += 1
        self.box.set_val(str(self.value))

    def decrease_step(self, event):
        if self.value > 1:
            self.value -= 1
            self.box.set_val(str(self.value))


class SyncButton:
    """
    This class represents the CheckButton used to enable or disable synchronization of slices across different dimensions.
    """

    def __init__(self, ax, trackers):
        # create CheckButtons widget
        self.button = CheckButtons(ax, ["Sync slices"], [False])
        self.status = self.button.get_status()

        # connect button to callback function
        self.button.on_clicked(self.on_click)
        self.trackers = trackers  # keep reference to all trackers

    def on_click(self, label):
        self.status = self.button.get_status()
        if self.status[0]:
            # synchronize to leftmost dimension being displayed
            if self.trackers:
                reference_tracker = self.trackers[0]  # leftmost tracker
                reference_index = reference_tracker.index
                for tracker in self.trackers:
                    tracker.index = reference_index
                    tracker.update()


def visualize_reconstruction(
    reconstruction, dimensions_to_display=[0, 1, 2], cmap="gray"
):
    """
    Visualizes reconstructed images from CT scans, supporting the viewing of 2D and 3D data
    with interactive controls for exploring different slices. It allows for the adjustment
    of display parameters including dimensions to display and colormap.

    Parameters
    ----------
    reconstruction : ndarray
        Reconstructed image data.
    dimensions_to_display : list of int, optional
        Specifies which dimensions to display, with (0, 1, 2) representing Axial,
        Coronal, and Sagittal planes. Defaults to all dimensions.
    cmap : str, optional
        Colormap for the image display. Default is "gray".

    Features
    --------
    - Interactive scrolling to navigate through slices.
    - Synchronization of slices across different dimensions.
    - Adjustable scroll step size for slice navigation.
    - Display of axial, coronal, and sagittal planes for 3D data.
    """

    reconstruction = np.array(reconstruction)

    # display single 2D image if data is not 3D or dimensions are not specified
    if reconstruction.ndim != 3 or dimensions_to_display is None:
        image = plt.imshow(reconstruction, cmap=cmap)
        colorbar(image)
        plt.show()
        return

    # validate dimensions to be displayed and ensure they are within expected range
    valid_dimensions = [i for i in range(3) if i in dimensions_to_display]
    number_of_dimensions = len(valid_dimensions)

    if number_of_dimensions == 0:
        print("No valid dimensions provided.")
        return

    # create subplots for each dimension
    fig, axs = plt.subplots(
        1, max(number_of_dimensions, 1), figsize=(6 * max(number_of_dimensions, 1), 6)
    )

    if number_of_dimensions > 1:
        plt.subplots_adjust(wspace=0.4)
    elif number_of_dimensions == 1:
        axs = [axs]

    dimension_names = ["Axial", "Coronal", "Sagittal"]
    trackers = []  # list to store SliceTracker instances for synchronization

    button_width = 0.3 / max(number_of_dimensions, 1)  # dynamically adjust button width
    button_height = 0.05
    button_x_start = 0.15 if number_of_dimensions > 1 else 0.25

    # create SyncButton for synchronizing slices across dimensions (only if multiple dimensions are displayed)
    sync_button_axis = (
        plt.axes([button_x_start, 0.875, button_width, button_height])
        if number_of_dimensions > 1
        else None
    )
    sync_button = (
        SyncButton(sync_button_axis, trackers) if number_of_dimensions > 1 else None
    )

    # setup ScrollStep controls for adjusting scrolling step size
    scroll_step_ax = plt.axes([button_x_start, 0.925, button_width, button_height])
    step_increase_ax = plt.axes(
        [button_x_start + button_width + 0.01, 0.95, button_width / 4, 0.025]
    )
    step_decrease_ax = plt.axes(
        [button_x_start + button_width + 0.01, 0.925, button_width / 4, 0.025]
    )
    scroll_step = ScrollStep(scroll_step_ax, step_increase_ax, step_decrease_ax)

    # initialize SliceTracker for each valid dimension to handle image display and interaction
    for i, dim in enumerate(valid_dimensions):
        ax_pos = axs[i].get_position()
        center_slice_button_axis = plt.axes(
            [
                ax_pos.x0
                + ax_pos.width / 2
                - button_width / 2,  # center center_slice_button
                ax_pos.y0 - button_height - 0.05,  # place below axis
                button_width,
                button_height,
            ]
        )
        tracker = SliceTracker(
            axs[i],
            np.flip(reconstruction, axis=1),  # flip data for correct orientation
            dim,
            cmap,
            center_slice_button_axis,
            dimension_names[dim],
            trackers,
            sync_button,
            scroll_step,
        )
        trackers.append(tracker)

    # connect scroll event for interactive slice scrolling functionality
    fig.canvas.mpl_connect(
        "scroll_event", lambda event: [t.on_scroll(event) for t in trackers]
    )

    plt.show()
