#include "doctest/doctest.h"

#include "Error.h"
#include "POGM.h"
#include "Identity.h"
#include "Logger.h"
#include "VolumeDescriptor.h"
#include "testHelpers.h"
#include "ConstantFunctional.h"

using namespace elsa;
using namespace doctest;

TEST_SUITE_BEGIN("solvers");

TEST_CASE_TEMPLATE("POGM: Solving a least squares problem", data_t, float, double)
{
    // Set seed for Eigen Matrices!
    srand((unsigned int) 666);

    // eliminate the timing info from console for the tests
    // Logger::setLevel(Logger::LogLevel::OFF);

    VolumeDescriptor volDescr({4, 7});

    Vector_t<data_t> bVec(volDescr.getNumberOfCoefficients());
    bVec.setRandom();
    DataContainer<data_t> b(volDescr, bVec);

    Identity<data_t> A(volDescr);

    data_t L = 1;
    POGM<data_t> solver(A, b, ZeroFunctional<data_t>{volDescr}, L);

    auto reco = solver.solve(50);

    CHECK_EQ(reco.squaredL2Norm(), doctest::Approx(b.squaredL2Norm()).epsilon(0.1));

    THEN("Clone is equal to original one")
    {
        auto clone = solver.clone();

        CHECK_EQ(*clone, solver);
        CHECK_NE(clone.get(), &solver);
    }
}

TEST_CASE_TEMPLATE("POGM: Solving a weighted least squares problem", data_t, float, double)
{
    // Set seed for Eigen Matrices!
    srand((unsigned int) 666);

    // eliminate the timing info from console for the tests
    // Logger::setLevel(Logger::LogLevel::OFF);

    VolumeDescriptor volDescr({4, 7});

    Vector_t<data_t> bVec(volDescr.getNumberOfCoefficients());
    bVec.setRandom();
    DataContainer<data_t> b(volDescr, bVec);

    Vector_t<data_t> wVec(volDescr.getNumberOfCoefficients());
    wVec.setOnes();
    DataContainer<data_t> W(volDescr, wVec);

    Identity<data_t> A(volDescr);

    data_t L = 1;
    POGM<data_t> solver(A, b, W, ZeroFunctional<data_t>{volDescr}, L);

    auto reco = solver.solve(50);

    CHECK_EQ(reco.squaredL2Norm(), doctest::Approx(b.squaredL2Norm()).epsilon(0.1));

    THEN("Clone is equal to original one")
    {
        auto clone = solver.clone();

        CHECK_EQ(*clone, solver);
        CHECK_NE(clone.get(), &solver);
    }
}

TEST_SUITE_END();
