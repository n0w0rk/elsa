#pragma once

#include "FFTPolicy.h"

#ifdef ELSA_CUDA_TOOLKIT_PRESENT
#include <cuda_runtime.h>
#include <cufftXt.h>

#include <thrust/universal_ptr.h>
#include <thrust/functional.h>
#include <thrust/transform.h>
#include <thrust/iterator/constant_iterator.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/gather.h>
#include <thrust/execution_policy.h>

#include <list>
#include <unordered_map>
#include <optional>
#endif

#include "Complex.h"
#include "elsaDefines.h"
#include "Error.h"
#include "DataDescriptor.h"
#include "VolumeDescriptor.h"
#include "ContiguousStorage.h"
#include "DataContainer.h"

#if WITH_FFTW
#define EIGEN_FFTW_DEFAULT
#endif
#include <unsupported/Eigen/FFT>

namespace elsa
{
    namespace detail
    {
        enum class FFTType : uint8_t {
            C2C,
            R2C,
            C2R,
            INVALID,
        };

        template <FFTType tvalue>
        struct FFTType_t {
            static constexpr FFTType value = tvalue;
        };

#ifdef ELSA_CUDA_TOOLKIT_PRESENT
        template <FFTType fft_type, typename inner>
        struct FFTCuFFTType {
            /* not valid */
            static constexpr cufftType value = CUFFT_C2C;
        };

        template <>
        struct FFTCuFFTType<FFTType::C2C, float> {
            static constexpr cufftType value = CUFFT_C2C;
        };

        template <>
        struct FFTCuFFTType<FFTType::C2C, double> {
            static constexpr cufftType value = CUFFT_Z2Z;
        };

        template <>
        struct FFTCuFFTType<FFTType::R2C, float> {
            static constexpr cufftType value = CUFFT_R2C;
        };

        template <>
        struct FFTCuFFTType<FFTType::R2C, double> {
            static constexpr cufftType value = CUFFT_D2Z;
        };

        template <>
        struct FFTCuFFTType<FFTType::C2R, float> {
            static constexpr cufftType value = CUFFT_C2R;
        };

        template <>
        struct FFTCuFFTType<FFTType::C2R, double> {
            static constexpr cufftType value = CUFFT_Z2D;
        };
#endif

        template <typename in, typename out>
        struct FFTInfo {
            using in_t = in;
            using out_t = out;
            using real_t =
                std::conditional_t<std::is_same_v<value_type_of_t<in>, value_type_of_t<out>>,
                                   value_type_of_t<in>, void>;
            using complex_t = std::conditional_t<is_complex_v<in>, in, out>;
            using eigen_in_t =
                std::conditional_t<is_complex_v<in>, std::complex<value_type_of_t<in>>, in>;
            using eigen_out_t =
                std::conditional_t<is_complex_v<out>, std::complex<value_type_of_t<out>>, out>;
            static constexpr FFTType kind =
                                         std::conditional_t < is_complex_v<in> && is_complex_v<out>,
                                     FFTType_t<FFTType::C2C>, std::conditional_t < is_complex_v<in>,
                                     FFTType_t<FFTType::C2R>,
                                     std::conditional_t < is_complex_v<out>,
                                     FFTType_t<FFTType::R2C>,
                                     FFTType_t < FFTType::INVALID >>>> ::value;
            static constexpr bool valid = !std::is_same_v<real_t, void> && kind != FFTType::INVALID;
#ifdef ELSA_CUDA_TOOLKIT_PRESENT
            /* cuFFT can only work with single precision and double precision floats; Since pointers
             * have to be cast to the corresponding cufft type pointers, its important to make sure
             * the conversions are valid. (Eigen's FFT is templated, so I'd hope that it complains
             * when it is unhappy with its input types)
             */
            static constexpr bool cufft_valid =
                valid && (std::is_same_v<real_t, float> || std::is_same_v<real_t, double>);
            static constexpr cufftType cufft_type = FFTCuFFTType<kind, real_t>::value;
#endif
        };

        /// @return [out_shape, normalization_constant]
        template <FFTType type>
        std::pair<IndexVector_t, index_t>
            computeOutputShapeAndNormalization(const IndexVector_t& src_shape)
        {
            static_assert(type == FFTType::C2C || type == FFTType::C2R || type == FFTType::R2C,
                          "Invalid fft type!");
            if constexpr (type == FFTType::C2C) {
                return std::make_pair(src_shape, src_shape.prod());
            } else if constexpr (type == FFTType::C2R) {
                index_t last_dim = src_shape.size() - 1;
                IndexVector_t dst_shape(src_shape.size());
                dst_shape << src_shape.head(last_dim), (src_shape(last_dim) - 1) * 2;
                return std::make_pair(dst_shape, dst_shape.prod());
            } else if constexpr (type == FFTType::R2C) {
                index_t last_dim = src_shape.size() - 1;
                IndexVector_t dst_shape(src_shape.size());
                dst_shape << src_shape.head(last_dim), src_shape(last_dim) / 2 + 1;
                return std::make_pair(dst_shape, src_shape.prod());
            }
        }

#ifdef ELSA_CUDA_TOOLKIT_PRESENT

        /* Create a cufft handle and plan an FFT for the given type and dimensions.
         * This handle is to be freed via cufftDestroy(...)
         * Returns a result indicating whether creating the plan was successful. If not,
         * the plan is not initialized and does not need to be freed.
         */
        cufftResult createPlan(cufftHandle* plan, cufftType type, const IndexVector_t& shape);

        /* Normalize the result after an fft with the factor size or sqrt(size), depending on
           the flag applySqrt. */
        template <typename data_t>
        void fftNormalize(data_t* ptr, index_t size, index_t normalization_constant, bool applySqrt)
        {
            data_t normalizing_factor = static_cast<data_t>(
                applySqrt ? std::sqrt(normalization_constant) : normalization_constant);
            thrust::transform(thrust::device, ptr, ptr + size,
                              thrust::make_constant_iterator(normalizing_factor), ptr,
                              thrust::divides<data_t>());
        }

        /* Assuming row-major layout! */
        struct TransposeIndexFunctor3D : public thrust::unary_function<size_t, size_t> {
            size_t rows, columns, depth;

            __host__ __device__ TransposeIndexFunctor3D(size_t rows, size_t columns, size_t depth)
                : rows(rows), columns(columns), depth(depth)
            {
            }

            __host__ __device__ size_t operator()(size_t linear_index)
            {
                size_t src_row = linear_index / (columns * depth);
                size_t remainder = linear_index % (columns * depth);
                size_t src_column = remainder / depth;
                size_t src_depth = remainder % depth;

                return src_depth * (rows * columns) + src_column * rows + src_row;
            }
        };

        /* src and dst must not alias! */
        template <typename data_t>
        bool transposeDevice(const data_t* src, data_t* dst, const IndexVector_t& src_shape)
        {

            switch (src_shape.size()) {
                case 1:
                    /* no need to transpose, but must copy from src to dst */
                    thrust::copy(thrust::device, src, src + src_shape.prod(), dst);
                    return true;
                case 2: {
                    TransposeIndexFunctor3D functor(src_shape(0), src_shape(1), 1);
                    thrust::counting_iterator<size_t> indices(0);
                    auto transpose_index_iterator =
                        thrust::make_transform_iterator(indices, functor);
                    thrust::gather(thrust::device, transpose_index_iterator,
                                   transpose_index_iterator + src_shape.prod(), src, dst);
                    return true;
                }
                case 3: {
                    TransposeIndexFunctor3D functor(src_shape(0), src_shape(1), src_shape(2));
                    thrust::counting_iterator<size_t> indices(0);
                    auto transpose_index_iterator =
                        thrust::make_transform_iterator(indices, functor);
                    thrust::gather(thrust::device, transpose_index_iterator,
                                   transpose_index_iterator + src_shape.prod(), src, dst);
                    return true;
                }
                default:
                    return false;
            }
        }

        /* Cache is not thread safe, also plans should not be used across multiple threads at the
         * same time! Hence, we use a thread local instance. Potential optimizations, should the
         * caches GPU memory consumption ever be a problem: Aside from disabling the caching
         * mechanism, there are two potential optimizations.
         *  - Currently, when no new plan can be allocated, the cache is flushed. This could be
         * extended also flush the caches of all other threads.
         *  - cuFFT allows us to manage the work area memory. Before planning,
         * cufftSetAutoAllocation(false) could be called, then the work area can be explicitely set.
         * This way, all elements of the cache could share a work area, fitting the requirements of
         * the largest cached plan. This would increase the management overhead, but reduce memory
         * consumption. Currently, the cache has to few elements for this to be worth it.
         */
        class CuFFTPlanCache
        {
        private:
            using CacheElement = std::tuple<cufftHandle, IndexVector_t, cufftType>;
            using CacheList = std::list<CacheElement>;
            CacheList _cache;
            /* this should be very low, to conserve GPU memory!
               Initialized via ELSA_CUFFT_CACHE_SIZE */
            size_t _limit;

            void flush();
            void evict();

        public:
            CuFFTPlanCache();

            CuFFTPlanCache(const CuFFTPlanCache& other) = delete;
            CuFFTPlanCache& operator=(const CuFFTPlanCache& other) = delete;
            /* This performs linear search, because that should be less overhead than a
               map lookup for very small cache sizes*/
            std::optional<cufftHandle> get(cufftType type, const IndexVector_t& shape);
        };

        extern thread_local CuFFTPlanCache cufftCache;

        /* ATTENTION! Expects data to be layed out in ROW MAJOR order!
         *
         *             | in_data_t       | out_data_t      |
         *        -----+-----------------+-----------------+
         *         C2C | complex<float>  | complex<float>  |
         *         Z2Z | complex<double> | complex<double> |
         *         C2R | complex<float>  | float           |
         *         Z2D | complex<double> | double          |
         *         R2C | float           | complex<float>  |
         *         D2Z | double          | complex<float>  |
         *
         */
        template <bool is_forward, typename in_data_t, typename out_data_t>
        bool doFftDevice(in_data_t* in_data, out_data_t* out_data, const IndexVector_t& shape,
                         const IndexVector_t& out_shape, FFTNorm norm,
                         index_t normalization_constant)
        {
            /* According to this example:
             * https://github.com/NVIDIA/CUDALibrarySamples/blob/master/cuFFT/1d_c2c/1d_c2c_example.cpp
             * it is fine to reinterpret_cast std::complex to cufftComplex.
             * The same applies to thrust::complex, which is what elsa::complex maps to.
             */

            using info = FFTInfo<in_data_t, out_data_t>;
            if constexpr (!info::cufft_valid) {
                static_cast<void>(in_data);
                static_cast<void>(out_data);
                static_cast<void>(shape);
                static_cast<void>(out_shape);
                static_cast<void>(norm);
                static_cast<void>(normalization_constant);
                return false;
            } else {
                constexpr cufftType type = info::cufft_type;

                cufftHandle plan;
#if ELSA_CUFFT_CACHE_SIZE != 0
                std::optional<cufftHandle> planOpt = cufftCache.get(type, shape);
                if (planOpt) {
                    plan = *planOpt;
                } else {
                    return false;
                }
#else
                if (createPlan(&plan, type, shape) != CUFFT_SUCCESS) {
                    return false;
                }
#endif

                bool success;
                if constexpr (type == CUFFT_C2C) {
                    int direction = is_forward ? CUFFT_FORWARD : CUFFT_INVERSE;
                    /* cuFFT can handle in-place transforms */
                    success = cufftExecC2C(plan, reinterpret_cast<cufftComplex*>(in_data),
                                           reinterpret_cast<cufftComplex*>(out_data), direction)
                              == CUFFT_SUCCESS;
                } else if constexpr (type == CUFFT_Z2Z) {
                    int direction = is_forward ? CUFFT_FORWARD : CUFFT_INVERSE;
                    success =
                        cufftExecZ2Z(plan, reinterpret_cast<cufftDoubleComplex*>(in_data),
                                     reinterpret_cast<cufftDoubleComplex*>(out_data), direction)
                        == CUFFT_SUCCESS;
                } else if constexpr (type == CUFFT_R2C) {
                    static_assert(is_forward);
                    success = cufftExecR2C(plan, reinterpret_cast<cufftReal*>(in_data),
                                           reinterpret_cast<cufftComplex*>(out_data))
                              == CUFFT_SUCCESS;
                } else if constexpr (type == CUFFT_C2R) {
                    static_assert(!is_forward);
                    success = cufftExecC2R(plan, reinterpret_cast<cufftComplex*>(in_data),
                                           reinterpret_cast<cufftReal*>(out_data))
                              == CUFFT_SUCCESS;
                } else if constexpr (type == CUFFT_D2Z) {
                    static_assert(is_forward);
                    success = cufftExecD2Z(plan, reinterpret_cast<cufftDoubleReal*>(in_data),
                                           reinterpret_cast<cufftDoubleComplex*>(out_data))
                              == CUFFT_SUCCESS;
                } else if constexpr (type == CUFFT_Z2D) {
                    static_assert(!is_forward);
                    success = cufftExecZ2D(plan, reinterpret_cast<cufftDoubleComplex*>(in_data),
                                           reinterpret_cast<cufftDoubleReal*>(out_data))
                              == CUFFT_SUCCESS;
                }
                cudaDeviceSynchronize();

                if (likely(success)) {
                    /* cuFFT performs unnormalized FFTs, therefore we are left to do scaling
                       according to FFTNorm */
                    if (norm == FFTNorm::FORWARD && is_forward || norm == FFTNorm::ORTHO
                        || norm == FFTNorm::BACKWARD && !is_forward) {
                        fftNormalize(out_data, out_shape.prod(), normalization_constant,
                                     norm == FFTNorm::ORTHO);
                    }
                }

#if ELSA_CUFFT_CACHE_SIZE == 0
                cufftDestroy(plan);
#endif

                return success;
            }
        }

        /* Perform am fft on the GPU using cuFFT if possible.
         * Template parameter is_forward determines whether an fft or ifft is performed.
         * true -> fft
         * false -> ifft
         *
         * A return value of false indicates that the operation was not successful and the
         * input data is unchanged.
         */
        template <class data_t, bool is_forward>
        bool fftDevice(thrust::universal_ptr<data_t> this_data, const IndexVector_t& src_shape,
                       FFTNorm norm)
        {
            /* Rationale: cuFFT expects the data in row major layout, but DataContainer uses column
               major layout (and the CPU dft implementation expects it, as well)
               => We use the following identity: DFT(A^T)^T = DFT(A) (the proof is left as an
               exercise to the reader ;)) Hence, the data does not need to be transposed, as long as
               the shape is reversed, because to cuFFT our input is transposed. */
            IndexVector_t src_shape_transposed = src_shape.reverse();
            auto [_, normalization_constant] =
                computeOutputShapeAndNormalization<FFTType::C2C>(src_shape_transposed);

            return doFftDevice<is_forward>(this_data.get(), this_data.get(), src_shape_transposed,
                                           src_shape_transposed, norm, normalization_constant);
        }

        /* shape is the actual size of the volume being transformed, not the size of the output
         * (which is less due to the symmetry that R2C exploits) */
        template <class data_t>
        bool rfftDevice(thrust::universal_ptr<const data_t> in_data, const IndexVector_t& shape,
                        const IndexVector_t& out_shape,
                        thrust::universal_ptr<elsa::complex<data_t>> out_data, FFTNorm norm,
                        index_t normalization_constant)
        {
            using info = FFTInfo<data_t, elsa::complex<data_t>>;
            if constexpr (!info::cufft_valid) {
                /* only single and double precision floats are supported */
                static_cast<void>(in_data);
                static_cast<void>(shape);
                static_cast<void>(out_shape);
                static_cast<void>(out_data);
                static_cast<void>(norm);
                static_cast<void>(normalization_constant);
                return false;
            }

            elsa::complex<data_t>* wc_buf;
            if (cudaMalloc(&wc_buf, sizeof(elsa::complex<data_t>) * out_shape.prod())
                != cudaSuccess) {
                return false;
            }

            /* making use of the fact that, for R2C, the input is at most as big as the output*/
            transposeDevice(in_data.get(), reinterpret_cast<data_t*>(out_data.get()), shape);
            bool success = doFftDevice<true>(reinterpret_cast<data_t*>(out_data.get()), wc_buf,
                                             shape, out_shape, norm, normalization_constant);

            if (success) {
                transposeDevice(wc_buf, out_data.get(), out_shape.reverse());
            }
            cudaFree(wc_buf);
            return success;
        }

        template <class data_t>
        bool irfftDevice(thrust::universal_ptr<const elsa::complex<data_t>> in_data,
                         const IndexVector_t& src_shape, const IndexVector_t& out_shape,
                         thrust::universal_ptr<data_t> out_data, FFTNorm norm,
                         index_t normalization_constant)
        {
            using info = FFTInfo<elsa::complex<data_t>, data_t>;
            if constexpr (!info::cufft_valid) {
                /* only single and double precision are supported */
                static_cast<void>(in_data);
                static_cast<void>(src_shape);
                static_cast<void>(out_shape);
                static_cast<void>(out_data);
                static_cast<void>(norm);
                static_cast<void>(normalization_constant);
                return false;
            }

            elsa::complex<data_t>* wc_buf1;
            if (cudaMalloc(&wc_buf1, sizeof(elsa::complex<data_t>) * src_shape.prod())
                != cudaSuccess) {
                return false;
            }

            data_t* wc_buf2;
            if (cudaMalloc(&wc_buf2, sizeof(data_t) * out_shape.prod()) != cudaSuccess) {
                cudaFree(wc_buf1);
                return false;
            }

            transposeDevice(in_data.get(), wc_buf1, src_shape);

            bool success = doFftDevice<false>(wc_buf1, wc_buf2, out_shape, out_shape, norm,
                                              normalization_constant);

            cudaFree(wc_buf1);
            if (success) {
                transposeDevice(wc_buf2, out_data.get(), out_shape.reverse());
            }
            cudaFree(wc_buf2);
            return success;
        }
#endif

        template <bool is_forward, typename in_data_t, typename out_data_t>
        void fftHostSingleDim(const in_data_t* in_data, const IndexVector_t& in_shape,
                              out_data_t* out_data, const IndexVector_t& out_shape, index_t dim_idx,
                              FFTNorm norm)
        {
            using info = FFTInfo<in_data_t, out_data_t>;
            using InputVector_t = const Eigen::Matrix<in_data_t, Eigen::Dynamic, 1>;
            using OutputVector_t = Eigen::Matrix<out_data_t, Eigen::Dynamic, 1>;

            // jumps in the data for the current dimension's data
            // dim_size[0] * dim_size[1] * ...
            // 1 for dim_idx == 0.
            const index_t in_stride = in_shape.head(dim_idx).prod();
            const index_t out_stride = out_shape.head(dim_idx).prod();

            // number of coefficients for the current dimension
            const index_t in_dim_size = in_shape(dim_idx);
            const index_t out_dim_size = out_shape(dim_idx);
            index_t true_dim_size;
            if constexpr (info::kind == FFTType::R2C) {
                true_dim_size = in_dim_size;
            } else {
                true_dim_size = out_dim_size;
            }

            // number of coefficients for the other dimensions
            // this is the number of 1d-ffts we'll do
            // e.g. shape=[2, 3, 4] and we do dim_idx=2 (=shape 4)
            //   -> in_shape.prod() == 24 / 4 = 6 == 2*3
            const index_t other_dims_size = in_shape.prod() / in_dim_size;
            const index_t out_other_dims_size = out_shape.prod() / out_dim_size;
            if (unlikely(other_dims_size != out_other_dims_size
                         || (info::kind == FFTType::C2C && in_dim_size != out_dim_size)
                         || (info::kind == FFTType::C2R && in_dim_size != out_dim_size / 2 + 1)
                         || (info::kind == FFTType::R2C && in_dim_size / 2 + 1 != out_dim_size))) {
                // if input & output do not have the same shape (in a C2R or R2C transform),
                // only the transform axis may be different
                throw Error{"FFT has incompatible input & output dimensions"};
            }
#ifndef EIGEN_FFTW_DEFAULT
// when using eigen+fftw, this corrupts the memory, so don't parallelize.
// error messages may include:
// * double free or corruption (fasttop)
// * malloc_consolidate(): unaligned fastbin chunk detected
#pragma omp parallel for
#endif
            // do all the 1d-ffts along the current dimensions axis
            for (index_t i = 0; i < other_dims_size; ++i) {

                index_t in_ray_start = i;
                // each time i is a multiple of stride,
                // jump forward the current+previous dimensions' shape product
                // (draw an indexed 3d cube to visualize this)
                in_ray_start +=
                    (in_stride * (in_dim_size - 1)) * ((i - (i % in_stride)) / in_stride);

                index_t out_ray_start = i;
                out_ray_start +=
                    (out_stride * (out_dim_size - 1)) * ((i - (i % out_stride)) / out_stride);

                // this is one "ray" through the volume
                Eigen::Map<InputVector_t, Eigen::AlignmentType::Unaligned, Eigen::InnerStride<>>
                    input_map(in_data + in_ray_start, in_dim_size, Eigen::InnerStride<>(in_stride));

                Eigen::Map<OutputVector_t, Eigen::AlignmentType::Unaligned, Eigen::InnerStride<>>
                    output_map(out_data + out_ray_start, out_dim_size,
                               Eigen::InnerStride<>(out_stride));
                using inner_t = typename info::real_t;

                Eigen::FFT<inner_t> fft_op;

                // disable any scaling in eigen - normally it does 1/n for ifft
                fft_op.SetFlag(Eigen::FFT<inner_t>::Flag::Unscaled);
                // use half spectrum as input/output for complex-to-real/real-to-complex
                // transforms
                fft_op.SetFlag(Eigen::FFT<inner_t>::Flag::HalfSpectrum);

                Eigen::Matrix<typename info::eigen_in_t, Eigen::Dynamic, 1> fft_in{in_dim_size};
                Eigen::Matrix<typename info::eigen_out_t, Eigen::Dynamic, 1> fft_out{out_dim_size};

                // eigen internally copies the fwd input matrix anyway if
                // it doesn't have stride == 1
                fft_in = input_map.block(0, 0, in_dim_size, 1)
                             .template cast<const typename info::eigen_in_t>();

                if (unlikely(in_dim_size == 1)) {
                    // eigen kiss-fft crashes for size=1...
                    // TODO: resolve for R2C/C2R
                    if constexpr (info::kind == FFTType::C2C) {
                        fft_out = fft_in;
                    } else if constexpr (info::kind == FFTType::C2R) {
                        fft_out(0) = fft_in(0).real();
                    } else {
                        fft_out(0) = fft_in(0);
                    }
                } else {
                    // arguments for in and out _must not_ be the same matrix!
                    // they will corrupt wildly otherwise.
                    if constexpr (is_forward) {
                        fft_op.fwd(fft_out, fft_in);
                        if (norm == FFTNorm::FORWARD) {
                            fft_out /= true_dim_size;
                        } else if (norm == FFTNorm::ORTHO) {
                            fft_out /= std::sqrt(true_dim_size);
                        }
                    } else {
                        fft_op.inv(fft_out, fft_in);
                        if (norm == FFTNorm::BACKWARD) {
                            fft_out /= true_dim_size;
                        } else if (norm == FFTNorm::ORTHO) {
                            fft_out /= std::sqrt(true_dim_size);
                        }
                    }
                }

                // we can't directly use the map as fft output,
                // since Eigen internally just uses the pointer to
                // the map's first element, and doesn't respect stride at all..
                output_map.block(0, 0, out_dim_size, 1) = fft_out.template cast<out_data_t>();
            }
        }

        // Algorithm sketch for different FFTs:
        //
        // if(R2C)
        //     perform R2C FFTs along dimension N // input -> output
        //     start_dim = 0
        //     end_dim = N-1
        // else if(C2R)
        //     start_dim = 0
        //     end_dim = N-1
        // else
        //     start_dim = 0
        //     end_dim = N
        //
        // perform FFTs for dims start_dim..end_dim // C2C: input -> output -> ... output
        //                                          // C2R: input -> tmp -> ... tmp
        //                                          // R2C: output -> output -> ... output
        //
        // if(C2R)
        //     perform C2R FFTs along dimension N // tmp -> output
        //
        // ATTENTION: in_data == out_data is allowed, assuming the input & output buffer is large
        //            enough to hold the input and the output (not simultaneously). In- and output
        //            size differs for C2R & R2C transforms
        template <class in_data_t, class out_data_t, bool is_forward>
        void fftHost(const in_data_t* in_data, const IndexVector_t& in_shape, out_data_t* out_data,
                     const IndexVector_t& out_shape, index_t dims, FFTNorm norm)
        {
            using info = FFTInfo<in_data_t, out_data_t>;

            if constexpr (info::valid) {
                // TODO: fftw variant

                // R2C is implicitely forward, C2R is implicitely backward
                static_assert((info::kind != FFTType::R2C || is_forward)
                              && (info::kind != FFTType::C2R || !is_forward));

                // For C2C, half_spectrum_shape == in_shape == out_shape
                IndexVector_t half_spectrum_shape;
                if constexpr (info::kind == FFTType::C2R) {
                    half_spectrum_shape = in_shape;
                } else {
                    half_spectrum_shape = out_shape;
                }

                // exclusive
                index_t end_dim;
                const typename info::complex_t* current_input;
                typename info::complex_t* working_area;
                std::unique_ptr<typename info::complex_t[]> tmp;
                if constexpr (info::kind == FFTType::R2C) {
                    end_dim = dims - 1;
                    fftHostSingleDim<is_forward>(in_data, in_shape, out_data, out_shape, end_dim,
                                                 norm);
                    current_input = out_data;
                    working_area = out_data;
                } else if constexpr (info::kind == FFTType::C2R) {
                    end_dim = dims - 1;
                    current_input = in_data;
                    tmp = std::make_unique<typename info::complex_t[]>(in_shape.prod());
                    working_area = tmp.get();
                } else {
                    end_dim = dims;
                    current_input = in_data;
                    working_area = out_data;
                }

                // generalization of an 1D-FFT
                // walk over each dimension and 1d-fft one 'line' of data
                for (index_t dim_idx = 0; dim_idx < end_dim; ++dim_idx) {
                    fftHostSingleDim<is_forward>(current_input, half_spectrum_shape, working_area,
                                                 half_spectrum_shape, dim_idx, norm);
                    current_input = working_area;
                }

                if constexpr (info::kind == FFTType::C2R) {
                    fftHostSingleDim<is_forward>(current_input, in_shape, out_data, out_shape,
                                                 end_dim, norm);
                }
            } else {
                static_cast<void>(in_data);
                static_cast<void>(out_data);
                static_cast<void>(in_shape);
                static_cast<void>(out_shape);
                static_cast<void>(dims);
                static_cast<void>(norm);
                // TODO: more concrete error with specific types
                throw Error{"unsupported FFT types"};
            }
        }
    } // namespace detail

    template <class data_t>
    void fft(ContiguousStorage<data_t>& x, const DataDescriptor& desc, FFTNorm norm,
             FFTPolicy policy)
    {
        const auto& src_shape = desc.getNumberOfCoefficientsPerDimension();
        const auto& src_dims = desc.getNumberOfDimensions();

#ifdef ELSA_CUDA_TOOLKIT_PRESENT
        if (policy != FFTPolicy::HOST) {
            if (detail::fftDevice<data_t, true>(x.data(), src_shape, norm)) {
                return;
            }
        }
#endif
        if (policy != FFTPolicy::DEVICE) {
            detail::fftHost<data_t, data_t, true>(x.data().get(), src_shape, x.data().get(),
                                                  src_shape, src_dims, norm);
        } else {
            throw Error{"Cannot sucessfully execute FFT with policy {}", policy};
        }
    }

    template <class data_t>
    void ifft(ContiguousStorage<data_t>& x, const DataDescriptor& desc, FFTNorm norm,
              FFTPolicy policy)
    {
        const auto& src_shape = desc.getNumberOfCoefficientsPerDimension();
        const auto& src_dims = desc.getNumberOfDimensions();

#ifdef ELSA_CUDA_TOOLKIT_PRESENT
        if (policy != FFTPolicy::HOST) {
            if (detail::fftDevice<data_t, false>(x.data(), src_shape, norm)) {
                return;
            }
        }
#endif
        if (policy != FFTPolicy::DEVICE) {
            detail::fftHost<data_t, data_t, false>(x.data().get(), src_shape, x.data().get(),
                                                   src_shape, src_dims, norm);
        } else {
            throw Error{"Cannot sucessfully execute IFFT with policy {}", policy};
        }
    }

    template <typename data_t>
    DataContainer<complex<data_t>> rfft(const DataContainer<data_t>& dc, FFTNorm norm,
                                        FFTPolicy policy)
    {
        const auto& desc = dc.getDataDescriptor();
        const auto& src_shape = desc.getNumberOfCoefficientsPerDimension();

        auto [dst_shape, normalization_constant] =
            detail::computeOutputShapeAndNormalization<detail::FFTType::R2C>(src_shape);
        DataContainer<complex<data_t>> out_dc(*std::make_unique<VolumeDescriptor>(dst_shape));

#ifdef ELSA_CUDA_TOOLKIT_PRESENT
        if (policy != FFTPolicy::HOST) {
            if (detail::rfftDevice<data_t>(dc.storage().data(), src_shape, dst_shape,
                                           out_dc.storage().data(), norm, normalization_constant)) {
                return out_dc;
            }
        }
#endif
        if (policy != FFTPolicy::DEVICE) {
            detail::fftHost<data_t, complex<data_t>, true>(dc.storage().data().get(), src_shape,
                                                           out_dc.storage().data().get(), dst_shape,
                                                           src_shape.size(), norm);
            return out_dc;
        } else {
            throw Error{"Cannot sucessfully execute RFFT with policy {}", policy};
        }
    }

    template <typename data_t>
    DataContainer<data_t> irfft(const DataContainer<complex<data_t>>& dc, FFTNorm norm,
                                FFTPolicy policy)
    {
        const auto& desc = dc.getDataDescriptor();
        const auto& src_shape = desc.getNumberOfCoefficientsPerDimension();

        auto [dst_shape, normalization_constant] =
            detail::computeOutputShapeAndNormalization<detail::FFTType::C2R>(src_shape);
        DataContainer<data_t> out_dc(*std::make_unique<VolumeDescriptor>(dst_shape));

#ifdef ELSA_CUDA_TOOLKIT_PRESENT
        if (policy != FFTPolicy::HOST) {
            if (detail::irfftDevice<data_t>(dc.storage().data(), src_shape, dst_shape,
                                            out_dc.storage().data(), norm,
                                            normalization_constant)) {
                return out_dc;
            }
        }
#endif
        if (policy != FFTPolicy::DEVICE) {
            detail::fftHost<complex<data_t>, data_t, false>(dc.storage().data().get(), src_shape,
                                                            out_dc.storage().data().get(),
                                                            dst_shape, src_shape.size(), norm);
        } else {
            throw Error{"Cannot sucessfully execute IRFFT with policy {}", policy};
        }
        return out_dc;
    }

} // namespace elsa
